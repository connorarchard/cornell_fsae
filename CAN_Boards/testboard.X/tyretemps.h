/* 
 * File:   tyretemps.h
 * Author: ChristineH
 *
 * Created on January 20, 2015, 2:03 PM
 */

#ifndef TYRETEMPS_H
#define	TYRETEMPS_H

#ifdef	__cplusplus
extern "C" {
#endif

#define SA 0x41 //slave address

// Constants for calculating object temperature
#define TMP006_B0 -0.0000294
#define TMP006_B1 -0.00000057
#define TMP006_B2 0.00000000463
#define TMP006_C2 13.4
#define TMP006_TREF 298.15
#define TMP006_A2 -0.00001678
#define TMP006_A1 0.00175
#define TMP006_S0 6.4  // * 10^-14

// Configuration Settings
#define TMP006_CFG_RESET    0x8000
#define TMP006_CFG_MODEON   0x7000
#define TMP006_CFG_1SAMPLE  0x0000
#define TMP006_CFG_2SAMPLE  0x0200
#define TMP006_CFG_4SAMPLE  0x0400
#define TMP006_CFG_8SAMPLE  0x0600
#define TMP006_CFG_16SAMPLE 0x0800
#define TMP006_CFG_DRDYEN   0x0100
#define TMP006_CFG_DRDY     0x0080

// Registers to read thermopile voltage and sensor temperature
#define TMP006_VOBJ  0x00
#define TMP006_TAMB 0x01
#define TMP006_CONFIG 0x02

//Needed Functions in Main
UINT16 MemRead(UINT8 SlaveAddress, UINT8 reg);
void MemWrite(UINT8 SlaveAddress, UINT8 reg, UINT16 command);
void config_TMP006(UINT8 addr, UINT16 samples);
double readObjTempC(UINT8 addr);
void SendRequest(void);
void DummyCommand(unsigned char byte);


#ifdef	__cplusplus
}
#endif

#endif	/* TYRETEMPS_H */

