#include <stdio.h>
#include <stdlib.h>
#include <p18F2585.h>
#include <math.h>
#include <string.h>
#include <xc.h>
#include "SMBus.h"
#include "main.h"
#include "tyretemps.h"
#include <plib/i2c.h>

#define LED_LE0 LATCbits.LC0 //Chip select LED driver 0 (LED's)


//*****************************************************************************************************************
//										FUNCTION'S DEFINITIONS
//*****************************************************************************************************************
/*void MCUinit(void)
{
    //OSCTUNEbits.PLLEN = 0; //Disable PLL GLOBAL Clock 16MHz  /changed to disabled 2/6   ////////////**********************************ENABLE PLL FOR INTERNAL 16MZH
    //IO setting-up
    ADCON1=0b00001111;		 //All chanels are digital I/O

    //SMBus setting-up
    mSDA_HIGH();		     //The bus is in idle state
    mSCL_HIGH();			 //SDA and SCL are in high level from pull up resitors
    OpenI2C(MASTER, SLEW_OFF);
    SSPADD = 0x09; // check this

    //Timer 0
    T0CONbits.T08BIT = 0;
    T0CONbits.T0PS0 = 1; //set frequency to Fosc / 64
    T0CONbits.T0PS1 = 0;
    T0CONbits.T0PS2 = 1;
    T0CONbits.T0CS = 0;
    T0CONbits.PSA = 0;
    INTCONbits.TMR0IE = 1; // Turn on timer 0 interrupt
    T0CONbits.TMR0ON = 1;

    //Timer 1 only interrupt on overflow
    T1CONbits.RD16 = 1;
    T1CONbits.T1RUN = 0;
    T1CONbits.TMR1CS = 0; //internal clock /4
    T1CONbits.T1CKPS0 = 1; //Timer 1 prescale set to 8
    T1CONbits.T1CKPS1 = 1;
    T1CONbits.T1OSCEN = 0;
    T1CONbits.TMR1ON = 0;
    PIE1bits.TMR1IE = 1;
    T1CONbits.TMR1ON = 1;

    TRISC = 0x0;
    LED_LE0 = 0b0;

}*///End of init()

UINT16 MemRead(UINT8 SlaveAddress, UINT8 reg)
{
	unsigned int  data;				// Data storage (DataH:DataL)
	unsigned char Pec;				// PEC byte storage
	unsigned char DataL;			// Low data byte storage
	unsigned char DataH;			// High data byte storage
	unsigned char arr[6];			// Buffer for the sent bytes
	unsigned char PecReg;			// Calculated PEC byte storage
	unsigned char ErrorCounter;		// Defines the number of the attempts for communication with MLX90614

	ErrorCounter=0x00;				// Initialising of ErrorCounter

	do{
	repeat:
            stop_bit();					//If slave send NACK stop comunication
            --ErrorCounter;				//Pre-decrement ErrorCounter
            if(!ErrorCounter){			//ErrorCounter=0?
                  break;					//Yes,go out from do-while{}
            }
            start_bit();				//Start condition

            if(TX_byte(SlaveAddress)){	//Send SlaveAddress
                    goto	repeat;			//Repeat comunication again
            }
            /*LED_LE0 ^= 1;
            __delay_ms(10);
            __delay_ms(10);*/
            if(TX_byte(reg)){		//Send command
                    goto	repeat;			//Repeat comunication again
            }
            //stop_bit();

            __delay_us(10);

            start_bit();				//Repeated Start condition

            if(TX_byte(SlaveAddress | 0b1)){ //Send SlaveAddress
                    goto	repeat;         //Repeat comunication again
            }

            DataL=RX_byte(ACK);			//Read low data,master must send ACK
            DataH=RX_byte(NACK); 		//Read high data,master must send ACK
            //Pec=RX_byte(NACK);			//Read PEC byte, master must send NACK
            stop_bit();					//Stop condition


            arr[5]=SlaveAddress;		//
            arr[4]=reg; //cmd				//
            arr[3]=SlaveAddress;		//Load array arr
            arr[2]=DataL;				//
            arr[1]=DataH;				//
            arr[0]=0;					//
            PecReg=PEC_calculation(arr);//Calculate CRC
            Pec = PecReg;

	}while(PecReg != Pec);		//If received and calculated CRC are equal go out from do-while{}

	*((unsigned char *)(&data))=DataH;	   //
	*((unsigned char *)(&data)+1)=DataL ;  //data=DataH:DataL

	return data;
}

void MemWrite(UINT8 SlaveAddress, UINT8 reg, UINT16 command)
{
    	unsigned int  data;				// Data storage (DataH:DataL)
	unsigned char Pec;				// PEC byte storage
	unsigned char DataL;			// Low data byte storage
	unsigned char DataH;			// High data byte storage
	unsigned char arr[6];			// Buffer for the sent bytes
	unsigned char PecReg;			// Calculated PEC byte storage
	unsigned char ErrorCounter;		// Defines the number of the attempts for communication with MLX90614

	ErrorCounter=0x00;				// Initialising of ErrorCounter

	repeat:
            stop_bit();					//If slave send NACK stop comunication
            //--ErrorCounter;				//Pre-decrement ErrorCounter
            //if(!ErrorCounter){			//ErrorCounter=0?
              //      break;					//Yes,go out from do-while{}
            //}
            start_bit();				//Start condition

            if(TX_byte(SlaveAddress)){	//Send SlaveAddress
                    goto	repeat;			//Repeat comunication again
            }
            /*LED_LE0 ^= 1;
            __delay_ms(10);
            __delay_ms(10);*/
            if(TX_byte(reg)){		//Send command
                    goto	repeat;			//Repeat comunication again
            }
            if(TX_byte(command>>8)){		//Send command
                    goto	repeat;			//Repeat comunication again
            }
            if(TX_byte(command)){		//Send command
                    goto	repeat;			//Repeat comunication again
            }
            stop_bit();
}

//----------------------TMP006 functions------------------------------------

void config_TMP006(UINT8 addr, UINT16 samples){
    MemWrite(addr, TMP006_CONFIG, samples | TMP006_CFG_MODEON | TMP006_CFG_DRDYEN);
}

INT16 readRawDieTemperature(UINT8 addr){
    INT16 raw = MemRead(addr, TMP006_TAMB);
    if (raw == 0xFF) LED_LE0 ^=1;
    raw >>= 2;
    return raw;
}

INT16 readRawVoltage(UINT8 addr) {
    INT16 raw = MemRead(addr, TMP006_VOBJ);
    return raw;
}

// Calculate object temperature based on raw sensor temp and thermopile voltage
double readObjTempC(UINT8 addr)
{
  double Tdie = readRawDieTemperature(addr);
  double Vobj = readRawVoltage(addr);
  Vobj *= 156.25;  // 156.25 nV per LSB
  Vobj /= 1000000000; // nV -> V
  //subtract
  Tdie *= 0.03125; // convert to celsius
  Tdie += 273.15; // convert to kelvin

  // Equations for calculating temperature found in section 5.1 in the user guide
  double tdie_tref = Tdie - TMP006_TREF;
  double S = (1 + TMP006_A1*tdie_tref +
      TMP006_A2*tdie_tref*tdie_tref);
  S *= TMP006_S0;
  S /= 10000000;
  S /= 10000000;

  double Vos = TMP006_B0 + TMP006_B1*tdie_tref +
    TMP006_B2*tdie_tref*tdie_tref;

  double fVobj = (Vobj - Vos) + TMP006_C2*(Vobj-Vos)*(Vobj-Vos);

  double Tobj = sqrt(sqrt(Tdie * Tdie * Tdie * Tdie + fVobj/S));

  Tobj -= 273.15; // Kelvin -> *C
  return (10*Tobj);
}

// Caculate sensor temperature based on raw reading
double readDieTempC(UINT8 addr)
{
  double Tdie = readRawDieTemperature(addr);
  Tdie *= 0.03125; // convert to celsius
  return Tdie;
}

//---------------------------------------------------------------------------------------------
void SendRequest(void)
{
	mSCL_LOW();			//SCL 1 ____________|<-----80ms------->|______________
	__delay_ms(10);		// 	  0	            |__________________|
	mSCL_HIGH();
}


//---------------------------------------------------------------------------------------------
void DummyCommand(unsigned char byte)
{
	start_bit();		//Start condition
	TX_byte(byte);		//Send Slave Address or whatever,no need ACK checking
	stop_bit();			//Stop condition
}

//----------------------------------------------------------------------------------------------

/*int main(int argc, char** argv) {
    UINT8	SlaveAddress; 			//Contains device address
    float	object_temp;				//Contains calculated temperature in degrees Celsius
    float 	sensor_temp;
    unsigned int 	i;

    MCUinit();						  		//MCU initialization
    SlaveAddress= SA<<1;						//Set device address
    //command_obj= RAM_Access|RAM_Tobj1; 	    //Form RAM access command + RAM address
    //command_amb= 1;
    SendRequest();							//Switch to SMBus mode - this is need if module is in PWM mode only
    DummyCommand(SlaveAddress);				//This is need if Request Command is sent even when the module is in SMBus mode
    config_TMP006(SlaveAddress, TMP006_CFG_8SAMPLE);

    while(1) {
        //LED_LE0 ^= 1;
        object_temp = readObjTempC(SlaveAddress);
        //LED_LE0 ^= 1;
        sensor_temp = readDieTempC(SlaveAddress);
        //LED_LE0 ^= 1;
        for(i=0;i<10;i++){
            __delay_ms(10); // delay 1 second for every 4 samples per reading
        }
    }
    return 1;
}*/