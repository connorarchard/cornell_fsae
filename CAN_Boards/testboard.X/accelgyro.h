/* 
 * File:   accelgyro.h
 * Author: ChristineH
 *
 * Created on January 23, 2015, 4:09 PM
 */

#ifndef ACCELGYRO_H
#define	ACCELGYRO_H

#ifdef	__cplusplus
extern "C" {
#endif

    //*MPU6050 CONSTANTS************************************************************************************

//MPU-6050 registers
#define MPU_6050   (0x68<<1) //device address of MPU-6050; see datasheet
#define WHO_AM_I    0x75 //Register 117, holds upper 6 bits of MPU6050's 7-bit I2C address = 0x68
#define PWR_MGMT_1  0x6B //Register 107, used to wake the MPU from sleep mode
#define INT_ENABLE    0x38 //Register 56, used to enable interrupts on the MPU
#define INT_STATUS    0x3A //Register 58, the location of the MPU's interrupt flags

#define GYRO_CONFIG    0x1B //Register 27, used to trigger gyroscope self-test & configure gyroscopes' full scale range
#define FS_SEL_0    0x00 //FS_SEL[1:0] - 0 = 250 o/s
#define FS_SEL_1    0x08 //FS_SEL[1:0] - 1 = 500 o/s
#define FS_SEL_2    0x10 //FS_SEL[1:0] - 2 = 1000 o/s
#define FS_SEL_3    0x18 //FS_SEL[1:0] - 3 = 2000 o/s

#define GYRO_XOUT_H 0x43 //Register 67, contains [15:8] of 16 bit 2's complement value of most recent x axis gyroscope measurement
#define GYRO_XOUT_L 0x44 //Register 68, contains [7:0]  of 16 bit 2's complement value of most recent x axis gyroscope measurement
#define GYRO_YOUT_H 0x45 //Register 69, contains [15:8] of 16 bit 2's complement value of most recent y axis gyroscope measurement
#define GYRO_YOUT_L 0x46 //Register 70, contains [7:0]  of 16 bit 2's complement value of most recent y axis gyroscope measurement
#define GYRO_ZOUT_H 0x47 //Register 71, contains [15:8] of 16 bit 2's complement value of most recent z axis gyroscope measurement
#define GYRO_ZOUT_L 0x48 //Register 72, contains [7:0]  of 16 bit 2's complement value of most recent z axis gyroscope measurement

#define ACCEL_CONFIG 0x1C //Register 28
#define ACCEL_XOUT_H 0x3B
#define ACCEL_XOUT_L 0x3C
#define ACCEL_YOUT_H 0x3D
#define ACCEL_YOUT_L 0x3E
#define ACCEL_ZOUT_H 0x3F
#define ACCEL_ZOUT_L 0x40

//Needed Functions in Main
UINT16 mpu_read(UINT8 SlaveAddress, UINT8 reg);
void mpu_write(UINT8 SlaveAddress, UINT8 reg, UINT16 command);
void mpu_init(void);
void readRawGyroData(void);
void readRawAccelData(void);
void convertGyroData(void);
void convertAccelData(void);

#ifdef	__cplusplus
}
#endif

#endif	/* ACCELGYRO_H */

