/* 
 * File:   testboard.c
 * Author: ChristineH
 *
 * Created on September 20, 2014, 1:42 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <p18F2585.h>
#include <math.h>
#include "main.h"
#include <string.h>
#include <xc.h>
#include "can18xx8.h"
#include "tyretemps.h"
#include <plib/i2c.h>
#include "accelgyro.h"

#define AN0 0b00000
#define AN1 0b00001
#define AN3 0b00011
#define AN9 0b01001
#define LED_LE0 LATCbits.LC0 //Chip select LED driver 0 (LED's)

// boardID_sensorID
#define tireID0 0x0D //0-2
#define tireID1 0x01
#define tireID2 0x02
#define brakeID 0x03 //3
#define SGID0 0x04
#define SGID1 0x05
#define SGID2 0x06
#define accelxID 0x07 //7-A
#define accelyID 0x08
#define accelzID 0x09
#define gyroxID 0x0A
#define gyroyID 0x0B
#define gyrozID 0x0C

#define tires 0
#define brakes 1
#define strainGauges 2
#define accelGyro 3

#define tiresTime 200       //Update 5 times/sec
#define brakesTime 200      //Update 5 times/sec
#define strainTime 5        //Update 200 times/sec
#define accelGyroTime 5     //Update 200 times/sec

int timers[4];
unsigned char inputs[3] = {AN0, AN1, AN3};
int SGID[3] = {SGID0, SGID1, SGID2};
int tireID[3] = {tireID0, tireID1, tireID2};

/*
A timer interrupt will occur every 1ms,
to keep track of when each task should be
executed
*/
void interrupt ISR(void) {
    if(INTCONbits.TMR0IF == 1) {
        INTCONbits.TMR0IF = 0;
        T0CONbits.TMR0ON = 0;
        TMR0L = 0x05;
        T0CONbits.TMR0ON = 1;
        for(int i = 0; i < 4; i++) {
            if(timers[i] > 0) timers[i]--;
        }
    }
}

/* Helper function to send data through CAN */
void CANsendData(unsigned int value, int id){
    BYTE MessageData[2];

    //convert 16bit unsigned int to 8bit unsigned int
    MessageData[1] = value & 0xff;
    MessageData[0]= (value >> 8);
    
    if(CANIsTxReady()){
        CANSendMessage(id,MessageData,2,CAN_TX_PRIORITY_0 & CAN_TX_STD_FRAME & CAN_TX_NO_RTR_FRAME);
    }
}

/* Function to get and send tire temp values from i2c */

/* Function to get and send analog brake temp values */
void getBrakeTemp() {
    unsigned int sensorRead;
    unsigned int brakeTemp;

    ADCON0bits.CHS = AN9; // Set which input to read from
    ADCON0bits.GODONE = 0b1; // Start acquiring analog input
    while(ADCON0bits.GODONE); // Wait until acquisition finishes
    sensorRead = (ADRESH<<4) + (ADRESL>>4); // Combine into 16 bit unsigned int

    brakeTemp = (UINT16)(.14331*(float)sensorRead + 3.2624);

    //send data on CAN
    CANsendData(brakeTemp, brakeID);
}

/* Function to get and send analog strain gauge values */
void getStrainGauge() {
    unsigned int strainGauge[3];

    for(int i = 0; i < 3; i++) {
        ADCON0bits.CHS = inputs[i]; // Set which input to read from
        ADCON0bits.GODONE = 0b1; // Start acquiring analog input
        while(ADCON0bits.GODONE); // Wait until acquisition finishes
        strainGauge[i] = (ADRESH<<4) + (ADRESL>>4); // Combine into 16 bit unsigned int
        //send data on CAN
        CANsendData(strainGauge[i], SGID[i]);
    }
}

/* Function to get and send accel and gyro values from i2c */
void getTireTemps() {
    float tireTemps[3];
    int cnt = 0;
    UINT8 SlaveAddress;

    for(int i = 0x40; i < 0x42; i++){
        SlaveAddress = i<<1;
        tireTemps[cnt] = readObjTempC(SlaveAddress);
        CANsendData((UINT16)tireTemps[cnt], tireID[cnt]);
        cnt++;
    }
}

void getAccelGyro() {
    char data_ready = mpu_read(MPU_6050, INT_STATUS);  //Check if data is ready
    if ((data_ready & 0b1) == 1) {      //bit 0 is DAT_RDY_INT
        readRawGyroData();
        readRawAccelData();

        //Convert raw data to degrees
        convertGyroData();
        convertAccelData();

        CANsendData((UINT16)(100*accel_x), accelxID);
        CANsendData((UINT16)(100*accel_y), accelyID);
        CANsendData((UINT16)(100*accel_z), accelzID);

        CANsendData((UINT16)(100*gyro_x), gyroxID);
        CANsendData((UINT16)(100*gyro_y), gyroyID);
        CANsendData((UINT16)(100*gyro_z), gyrozID);
    }
}

/* Initializes timers */
void init_timers() {
  //Initialize Timer0 to count mS
  INTCONbits.TMR0IF = 0x0;  //Clear timer0 overflow interrupt flag
  INTCONbits.TMR0IE = 1;    //enable the timer0 by setting TRM0IE flag

  T0CONbits.T08BIT = 1;
  T0CONbits.T0CS = 0;
  T0CONbits.PSA = 0;
  T0CONbits.T0PS2 = 0;
  T0CONbits.T0PS1 = 1;
  T0CONbits.T0PS0 = 1;
  //TMR0H = 0x00;
  TMR0L = 0x05;
  T0CONbits.TMR0ON = 1;
  INTCONbits.GIE=1;         //global interrupt enable
}

/* Initialize i2c and CAN values */
void initConfig(void){
    UINT8 SlaveAddress;

    INTCON = 0x00; //Disable interrupts
    PORTA = 0x00; // Clear Port A output latches
    TRISAbits.TRISA0 = 0b1; // Set RA0 as input
    TRISAbits.TRISA1 = 0b1; // Set RA1 as input
    TRISAbits.TRISA3 = 0b1; // Set RA3 as input
    TRISBbits.TRISB4 = 0b1; // Set RA2 as input
    ADCON0 = 0x00; // Reset ADC select bits
    ADCON0bits.CHS = AN0; // Initialize analog input to AN0
    ADCON1bits.VCFG = 0b00; // Use AVdd
    ADCON2bits.ADFM = 0b0; // Left justified result
    ADCON2bits.ACQT = 0b111; // Acquisition time of 20Tad
    ADCON2bits.ADCS = 0b101; // Taq = 16 Tosc
    ADCON0bits.ADON = 0b1; // Turn ADC on
    TRISC = 0x00;

    //SMBus setting-up
    mSDA_HIGH();		     //The bus is in idle state
    mSCL_HIGH();			 //SDA and SCL are in high level from pull up resitors
    OpenI2C(MASTER, SLEW_OFF);
    SSPADD = 0x09; // check this

    /*for (int i = 0x40; i < 0x42; i++){
        SlaveAddress = i<<1;
        SendRequest();							//Switch to SMBus mode - this is need if module is in PWM mode only
        DummyCommand(SlaveAddress);				//This is need if Request Command is sent even when the module is in SMBus mode
        config_TMP006(SlaveAddress, TMP006_CFG_8SAMPLE);
    }*/

    mpu_init();

    short init_flag = CAN_CONFIG_SAMPLE_ONCE & CAN_CONFIG_PHSEG2_PRG_ON &
    CAN_CONFIG_DBL_BUFFER_ON &CAN_CONFIG_ALL_MSG & CAN_CONFIG_LINE_FILTER_OFF;
    CANInitialize(1, 1, 3, 2, 2,init_flag);
    CIOCON = 0x20;

    timers[tires] = tiresTime;
    timers[brakes] = brakesTime;
    timers[strainGauges] = strainTime;
    timers[accelGyro] = accelGyroTime;

    LED_LE0 = 0b0;
}

int main(int argc, char** argv) {
    initConfig();
    init_timers();
    //LED_LE0 = 0b1;
    //__delay_ms(20);
    //LED_LE0 = 0b0;
    //LED_LE0 = 0b1;
    while(1) {
        /*if(timers[brakes] == 0) {
            //LED_LE0 ^= 1;
            timers[brakes] = brakesTime;
            getBrakeTemp();
        }
        if (timers[tires] == 0){
            timers[tires] = tiresTime;
            getTireTemps();
        }*/
        if(timers[accelGyro] == 0) {
            //LED_LE0 ^= 1;
            timers[accelGyro] = accelGyroTime;
            getAccelGyro();
        }
    }
    return (EXIT_SUCCESS);
}

