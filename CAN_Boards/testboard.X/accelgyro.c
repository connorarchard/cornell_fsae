/*
 * File:   main.c
 * Author: taylorpritchard
 *
 * Created on January 19, 2015, 4:51 PM
 */

/*
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <p18F2585.h>
#include <math.h>
#include <string.h>
#include <xc.h>
#include "accelgyro.h"
#include "SMBus.h"
#include "main.h"
#include <plib/i2c.h>
#include "tyretemps.h"

//Debug flag
//Set to 0 if using UART to send over Bluetooth; 1 if using UART for PuTTY
#define DEBUG 0

//Bit macros
#define set_bit(address,bit) (address |= (1<<bit))
#define clear_bit(address,bit) (address &= ~(1<<bit))
#define toggle_bit(address,bit) (address ^= (1<<bit))
#define check_bit(address,bit) ((address & (1<<bit)) == (1<<bit))
#define LED LATCbits.LC0

//Raw measured gyro values
INT16 gyro_x_raw;
INT16 gyro_y_raw;
INT16 gyro_z_raw;

//Used for measuring gyro data
unsigned char data_ready = 0;
unsigned char gyro_x_upper;
unsigned char gyro_x_lower;
unsigned char gyro_y_upper;
unsigned char gyro_y_lower;
unsigned char gyro_z_upper;
unsigned char gyro_z_lower;

//Initial gyroscope values (gyro has offsets on startup)
INT16 gyro_x_offset;
INT16 gyro_y_offset;
INT16 gyro_z_offset;

//Absolute angles of the gyroscope
double x_angle = 0;
double y_angle = 0;

//Raw measured accel values
INT16 accel_x_raw;
INT16 accel_y_raw;
INT16 accel_z_raw;

unsigned char accel_x_upper;
unsigned char accel_x_lower;
unsigned char accel_y_upper;
unsigned char accel_y_lower;
unsigned char accel_z_upper;
unsigned char accel_z_lower;

//Gyroscope measurements (in degrees/sec)
double gyro_x = 0;
double gyro_y = 0;
double gyro_z = 0;

//Accelerometer measurements (in degrees/sec)
double accel_x = 0;
double accel_y = 0;
double accel_z = 0;

//************ MPU HELPER FUNCTIONS ***************

UINT16 mpu_read(UINT8 SlaveAddress, UINT8 reg)
{
    //unsigned int  data;             // Data storage (DataH:DataL)
    unsigned char Pec;              // PEC byte storage
    //unsigned char DataL;            // Low data byte storage
    unsigned char DataH;            // High data byte storage
    unsigned char arr[6];           // Buffer for the sent bytes
    unsigned char PecReg;           // Calculated PEC byte storage
    unsigned char ErrorCounter;     // Defines the number of the attempts for communication with MLX90614

    ErrorCounter=0x00;              // Initialising of ErrorCounter

    do
    {
    repeat:
            stop_bit();                 //If slave send NACK stop comunication
            --ErrorCounter;             //Pre-decrement ErrorCounter
            if(!ErrorCounter)
            {
                break;                //Yes,go out from do-while{}
            }
            start_bit();                //Start condition

            if(TX_byte(SlaveAddress))   //Send SlaveAddress
            {
                goto repeat;     //Repeat comunication again
            }
            /*LED_LE0 ^= 1;
            __delay_ms(10);
            __delay_ms(10);*/
            if(TX_byte(reg))
            {       //Send command
                goto repeat;         //Repeat comunication again
            }
            //stop_bit();

            __delay_us(10);

            start_bit();                //Repeated Start condition

            if(TX_byte(SlaveAddress | 0b1)) //Send SlaveAddress
            {
                goto repeat;          //Repeat comunication again
            }

            //DataL=RX_byte(ACK);         //Read low data,master must send ACK
            DataH=RX_byte(NACK);        //Read high data,master must send ACK
            //Pec=RX_byte(NACK);        //Read PEC byte, master must send NACK
            stop_bit();                 //Stop condition


            arr[5]=SlaveAddress;        //
            arr[4]=reg;                 //cmd
            arr[3]=SlaveAddress;        //Load array arr
            arr[2]=DataH;               //
            arr[1]=0;               //
            arr[0]=0;                   //
            PecReg=PEC_calculation(arr);//Calculate CRC
            Pec = PecReg;

    }while(PecReg != Pec);      //If received and calculated CRC are equal go out from do-while{}

    //*((unsigned char *)(&data))=DataH;     //
    //*((unsigned char *)(&data)+1)=DataL ;  //data=DataH:DataL

    return DataH;
}

void mpu_write(UINT8 SlaveAddress, UINT8 reg, UINT16 command)
{
    unsigned char ErrorCounter;     // Defines the number of the attempts for communication with MLX90614

    ErrorCounter=0x00;              // Initialising of ErrorCounter

    repeat:
            stop_bit();                 //If slave send NACK stop comunication
            //--ErrorCounter;               //Pre-decrement ErrorCounter
            //if(!ErrorCounter){            //ErrorCounter=0?
              //      break;                    //Yes,go out from do-while{}
            //}
            start_bit();                //Start condition

            if(TX_byte(SlaveAddress)) //Send SlaveAddress
            {
                goto repeat;         //Repeat comunication again
            }
            //LED ^= 0b1;
            if(TX_byte(reg)) //Send command
            {
                goto repeat;         //Repeat comunication again
            }
            if(TX_byte(command)) //Send command
            {
                goto repeat;         //Repeat comunication again
            }
            stop_bit();
}

/*
 * Concatenates the upper 8 bits & lower 8 bits into a 16-bit signed value.
 * Params:
 *        upper - the upper 8 bits
 *        lower - the lower 8 bits
 * Returns: a 16-bit signed int
 */
INT16 combine(unsigned char upper, unsigned char lower){
    INT16 result = 0;
    result = ((upper << 8) | lower);
    return result;
}

/*
 * Reads in the raw x, y, and z values of the gyroscope and puts them
 * in global variables gyro_*_raw.
 *
 * NOTE: This function does NOT check the Data Ready bit before reading
 * gyro data. It is up to the caller to do that before calling this.
 *
 * Returns: nothing
 */
void readRawGyroData() {
    gyro_x_upper = mpu_read(MPU_6050, GYRO_XOUT_H);
    gyro_x_lower = mpu_read(MPU_6050, GYRO_XOUT_L);
    gyro_x_raw   = combine(gyro_x_upper, gyro_x_lower);

    gyro_y_upper = mpu_read(MPU_6050, GYRO_YOUT_H);
    gyro_y_lower = mpu_read(MPU_6050, GYRO_YOUT_L);
    gyro_y_raw   = combine(gyro_y_upper, gyro_y_lower);

    gyro_z_upper = mpu_read(MPU_6050, GYRO_ZOUT_H);
    gyro_z_lower = mpu_read(MPU_6050, GYRO_ZOUT_L);
    gyro_z_raw   = combine(gyro_z_upper, gyro_z_lower);
}

/*
 * Takes in raw gyroscope x, y, and z values, converts them to actual measurements
 * in degrees/sec, and puts the values in global variables gyro_*.
 * Returns: nothing
 */
void convertGyroData() {
    char FS_SEL = (mpu_read(MPU_6050, GYRO_CONFIG) >> 3) | 0b11;
    double factor = 1 << FS_SEL;
    factor = 131/factor;

    gyro_x = ((double) (gyro_x_raw - gyro_x_offset)) / factor;
    gyro_y = ((double) (gyro_y_raw - gyro_y_offset)) / factor;
    gyro_z = ((double) (gyro_z_raw - gyro_z_offset)) / factor;
}

/*
 * Reads in the raw x, y, and z values of the accelerometer and puts them
 * in global variables accel_*_raw.
 *
 * NOTE: This function does NOT check the Data Ready bit before reading
 * accel data. It is up to the caller to do that before calling this.
 *
 * Returns: nothing
 */
void readRawAccelData() {
    accel_x_upper = mpu_read(MPU_6050, ACCEL_XOUT_H);
    accel_x_lower = mpu_read(MPU_6050, ACCEL_XOUT_L);
    accel_x_raw   = combine(accel_x_upper, accel_x_lower);

    accel_y_upper = mpu_read(MPU_6050, ACCEL_YOUT_H);
    accel_y_lower = mpu_read(MPU_6050, ACCEL_YOUT_L);
    accel_y_raw   = combine(accel_y_upper, accel_y_lower);

    accel_z_upper = mpu_read(MPU_6050, ACCEL_ZOUT_H);
    accel_z_lower = mpu_read(MPU_6050, ACCEL_ZOUT_L);
    accel_z_raw   = combine(accel_z_upper, accel_z_lower);
}


void convertAccelData(void){
    accel_x = ((double) (accel_x_raw)) / 4096.0;   //+- 8g for 4096
    accel_y = ((double) (accel_y_raw)) / 4096.0;
    accel_z = ((double) (accel_z_raw)) / 4096.0;

}
//Converts the already acquired accelerometer data into 3D euler angles
/*void convertAccel2Angle() {
    x_angle = 57.295*atan((float) accel_y_raw / sqrt(pow((float)accel_z_raw,2)+pow((float)accel_x_raw,2)));
    y_angle = 57.295*atan((float)-accel_x_raw / sqrt(pow((float)accel_z_raw,2)+pow((float)accel_y_raw,2)));
}*/

/*
 * Wakes up the MPU-6050 and initializes it with the proper settings.
 * Returns: nothing
 */
void mpu_init(void) {
    unsigned char sleep;
    unsigned char AFS_SEL;

    //Reset all registers on the MPU
    SendRequest();							//Switch to SMBus mode - this is need if module is in PWM mode only
    DummyCommand(MPU_6050);
    mpu_write(MPU_6050, PWR_MGMT_1, 0b10000000);

    for(int i=0;i<10;i++)
    {
        __delay_ms(10); // delay 1 second for every 4 samples per reading
    }

    //Enable the Data Ready interrupt
    mpu_write(MPU_6050, INT_ENABLE, 0b1); //bit 0 is DATA_RDY_EN

    //Wake the MPU from sleep mode
    mpu_write(MPU_6050, PWR_MGMT_1, 0);

    //Check that the MPU has been awoken
    sleep = mpu_read(MPU_6050, PWR_MGMT_1);

    //Give the MPU some time to adjust
    for(int i=0;i<10;i++)
    {
        __delay_ms(10); // delay 1 second for every 4 samples per reading
    }

    //Wait until the MPU has data ready
    while ((data_ready & 0b1) != 1) {       //bit 0 is DAT_RDY_INT
        data_ready = mpu_read(MPU_6050, INT_STATUS);  //Check if data is ready
    }

    //Calibrate gyro
    readRawGyroData();
    //gyro offsets are now in gyro_*raw

    //put the read values into offset
    gyro_x_offset = gyro_x_raw;
    gyro_y_offset = gyro_y_raw;
    gyro_z_offset = gyro_z_raw;

    AFS_SEL = mpu_read(MPU_6050, ACCEL_CONFIG);
    AFS_SEL |= 0b00010000;      //set bits 4-3 to 10
    AFS_SEL &= 0b11110111;
    mpu_write(MPU_6050, ACCEL_CONFIG, AFS_SEL);
}


//----------------------------------------------------------------------------------------------

//*************** MAIN *****************
/*int main(int argc, char** argv)
{
    mpu_init();

    //IMPORTANT: Make sure that twimaster.c is NOT in this project's folder.

    while(1)
    {
        //if (buttonPressed == 0) {
            data_ready = mpu_read(MPU_6050, INT_STATUS);  //Check if data is ready
            if ((data_ready & 0b1) == 1) {      //bit 0 is DAT_RDY_INT

                readRawGyroData();
                readRawAccelData();

                //Convert raw data to degrees
                convertGyroData();
                convertAccelData();

            }
        //}
        for(int i=0;i<20;i++)
        {
            __delay_ms(10); // delay 1 second for every 4 samples per reading
        }
    }
    return 1;
}*/


