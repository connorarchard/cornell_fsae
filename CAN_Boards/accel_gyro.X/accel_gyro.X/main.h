/* Uncomment this if push pull clock need to be used */
//#define PUSH_PULL

/*  If bootloader is not desired do next:
	1. Comment #define BOOTLOADER
	2. Remove from the project BLOAD_PM.asm file
	3. In c018i.c file replace "#pragma code _entry_scn=0x000200"
						with   "#pragma code _entry_scn=0x000000"
	4. Replace rm18F4320i.lkr file with 18F4320i.lkr
*/
//#define BOOTLOADER

// Set bit in a variable
#define bit_set(var,bitno) ((var) |= 1 << (bitno))
// Clear bit in a variable
#define bit_clr(var,bitno) ((var) &= ~(1 << (bitno)))
// Test bit in a variable
#define testbit(data,bitno) ((data>>bitno)&0x01)

//SMBus control signals
#define _SCL_IO TRISCbits.TRISC3
#define _SDA_IO TRISCbits.TRISC4
#define _SCL    PORTCbits.RC3
#define _SDA    PORTCbits.RC4


//Delay constants
#define DEL1SEC	 100000
#define DEL80ms	 7400
#define DEL200ms 18500
#define TBUF	 2

#define mSDA_HIGH()	_SDA_IO=1;
#define mSDA_LOW()  _SDA=0;_SDA_IO=0;

#ifndef PUSH_PULL
	#define mSCL_HIGH()	_SCL_IO=1;
#else
	#define mSCL_HIGH()	_SCL=1;_SCL_IO=0;
#endif

#define mSCL_LOW()  _SCL=0;_SCL_IO=0;

#define ACK	 0
#define	NACK 1


//main initialize
#pragma config OSC =  HS  // IRCIO67//IRCIO7  //this now enables high speed oscillator //IRCIO7 <- This was the old value for internal with ouput enabled
#pragma config PWRT = ON//// TURNS ON power on timer to allow psu to be stabl

#define _XTAL_FREQ 16000000

//*MPU6050 CONSTANTS************************************************************************************

//MPU-6050 registers
#define MPU_6050   (0x68<<1) //device address of MPU-6050; see datasheet
#define WHO_AM_I    0x75 //Register 117, holds upper 6 bits of MPU6050's 7-bit I2C address = 0x68
#define PWR_MGMT_1  0x6B //Register 107, used to wake the MPU from sleep mode
#define INT_ENABLE    0x38 //Register 56, used to enable interrupts on the MPU
#define INT_STATUS    0x3A //Register 58, the location of the MPU's interrupt flags

#define GYRO_CONFIG    0x1B //Register 27, used to trigger gyroscope self-test & configure gyroscopes' full scale range
#define FS_SEL_0    0x00 //FS_SEL[1:0] - 0 = 250 o/s
#define FS_SEL_1    0x08 //FS_SEL[1:0] - 1 = 500 o/s
#define FS_SEL_2    0x10 //FS_SEL[1:0] - 2 = 1000 o/s
#define FS_SEL_3    0x18 //FS_SEL[1:0] - 3 = 2000 o/s

#define GYRO_XOUT_H 0x43 //Register 67, contains [15:8] of 16 bit 2's complement value of most recent x axis gyroscope measurement
#define GYRO_XOUT_L 0x44 //Register 68, contains [7:0]  of 16 bit 2's complement value of most recent x axis gyroscope measurement
#define GYRO_YOUT_H 0x45 //Register 69, contains [15:8] of 16 bit 2's complement value of most recent y axis gyroscope measurement
#define GYRO_YOUT_L 0x46 //Register 70, contains [7:0]  of 16 bit 2's complement value of most recent y axis gyroscope measurement
#define GYRO_ZOUT_H 0x47 //Register 71, contains [15:8] of 16 bit 2's complement value of most recent z axis gyroscope measurement
#define GYRO_ZOUT_L 0x48 //Register 72, contains [7:0]  of 16 bit 2's complement value of most recent z axis gyroscope measurement

#define ACCEL_CONFIG 0x1C //Register 28
#define ACCEL_XOUT_H 0x3B
#define ACCEL_XOUT_L 0x3C
#define ACCEL_YOUT_H 0x3D
#define ACCEL_YOUT_L 0x3E
#define ACCEL_ZOUT_H 0x3F
#define ACCEL_ZOUT_L 0x40

//*EXTERNAL FUNCTIONS**********************************************************************************************
extern 	void start_bit(void);
extern	void stop_bit(void);
extern	unsigned char TX_byte(unsigned char Tx_buffer);
extern	unsigned char RX_byte(unsigned char ack_nack);

//*PROTOTYPES*****************************************************************************************
void MCUinit(void);
UINT16 MemRead(UINT8 SlaveAddress, UINT8 reg);
void SendRequest(void);
void DummyCommand(unsigned char byte);
int CalcTemp(unsigned int value);
extern  unsigned char PEC_calculation(unsigned char pec[]);