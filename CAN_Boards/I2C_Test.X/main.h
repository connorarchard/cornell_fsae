/* Uncomment this if push pull clock need to be used */
//#define PUSH_PULL

/*  If bootloader is not desired do next:
	1. Comment #define BOOTLOADER
	2. Remove from the project BLOAD_PM.asm file
	3. In c018i.c file replace "#pragma code _entry_scn=0x000200"
						with   "#pragma code _entry_scn=0x000000"
	4. Replace rm18F4320i.lkr file with 18F4320i.lkr
*/
//#define BOOTLOADER

// Set bit in a variable
#define bit_set(var,bitno) ((var) |= 1 << (bitno))
// Clear bit in a variable
#define bit_clr(var,bitno) ((var) &= ~(1 << (bitno)))
// Test bit in a variable
#define testbit(data,bitno) ((data>>bitno)&0x01)

//SMBus control signals
#define _SCL_IO TRISCbits.TRISC3
#define _SDA_IO TRISCbits.TRISC4
#define _SCL    PORTCbits.RC3
#define _SDA    PORTCbits.RC4

#define mSDA_HIGH()	_SDA_IO=1;
#define mSDA_LOW()  _SDA=0;_SDA_IO=0;

#ifndef PUSH_PULL
	#define mSCL_HIGH()	_SCL_IO=1;
#else
	#define mSCL_HIGH()	_SCL=1;_SCL_IO=0;
#endif

#define mSCL_LOW()  _SCL=0;_SCL_IO=0;

#define ACK	 0
#define	NACK 1

//Delay constants
#define DEL1SEC	 100000
#define DEL80ms	 7400
#define DEL200ms 18500
#define TBUF	 2

//main initialize
#pragma config OSC =  HS  // IRCIO67//IRCIO7  //this now enables high speed oscillator //IRCIO7 <- This was the old value for internal with ouput enabled
#pragma config PWRT = ON//// TURNS ON power on timer to allow psu to be stabl

#define _XTAL_FREQ 16000000

#define SA 0x41 //slave address

// Constants for calculating object temperature
#define TMP006_B0 -0.0000294
#define TMP006_B1 -0.00000057
#define TMP006_B2 0.00000000463
#define TMP006_C2 13.4
#define TMP006_TREF 298.15
#define TMP006_A2 -0.00001678
#define TMP006_A1 0.00175
#define TMP006_S0 6.4  // * 10^-14

// Configuration Settings
#define TMP006_CFG_RESET    0x8000
#define TMP006_CFG_MODEON   0x7000
#define TMP006_CFG_1SAMPLE  0x0000
#define TMP006_CFG_2SAMPLE  0x0200
#define TMP006_CFG_4SAMPLE  0x0400
#define TMP006_CFG_8SAMPLE  0x0600
#define TMP006_CFG_16SAMPLE 0x0800
#define TMP006_CFG_DRDYEN   0x0100
#define TMP006_CFG_DRDY     0x0080

// Registers to read thermopile voltage and sensor temperature
#define TMP006_VOBJ  0x00
#define TMP006_TAMB 0x01
#define TMP006_CONFIG 0x02

//*EXTERNAL FUNCTIONS**********************************************************************************************
extern 	void start_bit(void);
extern	void stop_bit(void);
extern	unsigned char TX_byte(unsigned char Tx_buffer);
extern	unsigned char RX_byte(unsigned char ack_nack);

//*PROTOTYPES*****************************************************************************************
void MCUinit(void);
UINT16 MemRead(UINT8 SlaveAddress, UINT8 reg);
void SendRequest(void);
void DummyCommand(unsigned char byte);
int CalcTemp(unsigned int value);
extern  unsigned char PEC_calculation(unsigned char pec[]);