/****************************************************
*       This file was created by                    *
*       Microchip Application Maestro               *
*       and holds the set-up parameters             *
*       specified when the code was generated.      *
*       The user is advised not to make any         *
*       changes to this file                        *
****************************************************/
#ifndef CANDEF_H
#define CANDEF_H

#define BAUD_RATE_PRESC 1 //This value gets subtracted by one before passing to config
#define SJW_TIME 1 //described to be "enough" for our non ceramic oscillators
#define SAMPLES 1
#define PROP_TIME 2
#define PH_SEG_1 3
#define PH_SEG_2 2
#define CAN_CONFIG_1 BAUD_RATE_PRESC-1|(SJW_TIME-1<<6)
#if SAMPLES == 1
#define CAN_CONFIG_2 0x80|(PH_SEG_1-1<<3)|(PROP_TIME-1)
#elif SAMPLES == 3
#define CAN_CONFIG_2 0xC0|(PH_SEG_1-1<<3)|(PROP_TIME-1)
#else
#error "Number of samples is out of range"
#endif
#define CAN_CONFIG_3 PH_SEG_2-1
#define RXMASK0 0x7f0 //mask top 7 bits of addr
#define RXMASK1 0x7f0 //mask top 7 bits of addr
#define RXFILT0 0x5f0 //mask and filter combo will allow 1520-1535 addr
#define ST_FILTER_0
#define RXFILT1 0x5f0
#define ST_FILTER_1
#define RXFILT2 0x5f0
#define ST_FILTER_2
#define RXFILT3 0x5f0
#define ST_FILTER_3
#define RXFILT4 0x5f0
#define ST_FILTER_4
#define RXFILT5 0x5f0
#define ST_FILTER_5
//#define LPBACK
#define MY_IDENT 0x12c //my address is 300
#define STD_IDENT
#define RX_BUFFER 4
#define TX_BUFFER 4
//#define CAN_INT_LOW
//#define CAN_ERROR_HANDLER_ENABLE

