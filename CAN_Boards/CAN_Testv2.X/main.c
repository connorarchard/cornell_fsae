/* 
 * File:   main.c
 * Author: en-fsae
 *
 * Created on September 19, 2014, 6:00 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <p18F2585.h>
#include <math.h>
#include <string.h>
#include <xc.h>
#include"can18xx8.h"

#pragma config OSC =  HS  // IRCIO67//IRCIO7  //this now enables high speed oscillator //IRCIO7 <- This was the old value for internal with ouput enabled

#pragma config PWRT = ON//// TURNS ON power on timer to allow psu to be stabl

#define _XTAL_FREQ 16000000
#define LED LATCbits.LC0
#define CHECK LATBbits.LATB0

unsigned int LED_timer; // in mS
unsigned int CAN_timer; // in mS

void init_timers() {
  //Initialize Timer0 to count mS
  INTCONbits.GIE=1;         //globle interrupt enable
  INTCONbits.TMR0IF = 0x0;  //Clear timer0 overflow interrupt flag
  INTCONbits.TMR0IE = 1;    //enable the timer0 by setting TRM0IE flag

  T0CONbits.T08BIT = 1;
  T0CONbits.T0CS = 0;
  T0CONbits.PSA = 0;
  T0CONbits.T0PS2 = 0;
  T0CONbits.T0PS1 = 1;
  T0CONbits.T0PS0 = 1;
  //TMR0H = 0x00;
  TMR0L = 0x05;
  T0CONbits.TMR0ON = 1;
}


void initialize(void) {

    /* USE FOR INTERNAL OSCILLATOR ONLY*/
//    OSCTUNEbits.INTSRC = 1;     //31.25 kHz device clock derived from 8 MHz INTOSC source (divide-by-256 enabled
//    OSCCONbits.IRCF0 = 0;          //4 MHz (INTOSC drives clock directly)
//    OSCCONbits.IRCF1 = 1;
//    OSCCONbits.IRCF2 = 1;
//
//    OSCCONbits.SCS = 0;
//
//    while(OSCCONbits.OSTS == 0){}    //Oscillator Start-up Time-out Status bit
//    while(OSCCONbits.IOFS == 0){}    //INTOSC Frequency Stable bit
//
//    OSCTUNEbits.PLLEN = 1;         //PLL enabled for INTOSC (4 MHz and 8 MHz only)
//
//    while(OSCCONbits.IOFS == 0){}    //INTOSC Frequency Stable bit

    TRISC = 0x00;
    TRISB = 0x08;
    LED = 0b0;
    LED_timer = 1000; //LED toggles every second
    CAN_timer = 20;  //Try to send CAN message every 200 mS
    short init_flag = CAN_CONFIG_SAMPLE_ONCE & CAN_CONFIG_PHSEG2_PRG_ON & CAN_CONFIG_DBL_BUFFER_ON &CAN_CONFIG_ALL_MSG & CAN_CONFIG_LINE_FILTER_OFF;
    CANInitialize(1, 1, 3, 2, 2,init_flag);
    //CANSetOperationMode(CAN_OP_MODE_CONFIG, 0xFF);
    //short mask = -1;
    //CANSetMask(CAN_MASK_B1, mask, CAN_CONFIG_STD_MSG)
    init_timers();
    CIOCON = 0x20;
}

// interrupt handler for the timer0 overflow
void interrupt ISR(void) {
  // Reset the timer count
  TMR0L=0x05;
  // Reset interrupt flag
  INTCONbits.TMR0IF = 0;
  // Decrement Timers
  if (CAN_timer > 0) CAN_timer--;
  //LED = 1 - LED;
//  LED_timer--;
}

int main(int argc, char** argv) {
    initialize();
    BYTE MessageData[8];    //1 byte to send
    for (int i = 0; i<8; i++){
                    MessageData[i] = 0xAA;
                }
    while(1) {
        if (CAN_timer == 0) {
            CAN_timer = 20;
            //TXB0CONbits.TXREQ = 0;
            //TXB1CONbits.TXREQ = 0;
            //TXB2CONbits.TXREQ = 0;
            if(CANIsTxReady()){
                LED^=1;
                CANSendMessage(0x12c,MessageData,8,CAN_TX_PRIORITY_0 & CAN_TX_STD_FRAME & CAN_TX_NO_RTR_FRAME);
        
            }        }
    }
    return 1;
}

