function [T]= track_gen(WoT)
% Imports track dimensions & plots shape of track

% Import track dimensions from excel file
%[T,~,~]= xlsread(track);
T = [
         0   25.0000         0;
    1.0000   14.3000   35.0000;
   -1.0000   14.3000   65.0000;
    1.0000   14.3000   65.0000;
         0   10.0000         0;
   -1.0000   11.0000   50.0000;
    1.0000   11.0000   50.0000;
   -1.0000   11.0000   50.0000;
    1.0000   11.0000   50.0000;
         0   30.0000         0;
   -1.0000    7.1000   80.0000;
         0   10.0000         0;
    1.0000   14.3000   44.0000;
         0   60.0000         0;
   -1.0000   12.5000   25.0000;
    1.0000   12.5000   50.0000;
   -1.0000   12.5000   50.0000;
    1.0000   12.5000   50.0000;
   -1.0000   12.5000   50.0000;
    1.0000   12.5000   50.0000;
         0   15.0000         0;
   -1.0000   16.7000   76.0000;
    1.0000   16.7000   58.0000;
         0   45.0000         0;
    1.0000   14.3000   68.0000;
         0   10.0000         0;
   -1.0000   20.0000   32.0000;
         0   15.0000         0;
    1.0000    8.3000  145.0000;
         0   20.0000         0;
    1.0000   10.0000   83.0000;
   -1.0000   10.0000   83.0000;
         0   70.0000         0;
   -1.0000   20.0000   41.0000;
    1.0000   20.0000   41.0000;
   -1.0000   20.0000   41.0000;
    1.0000   20.0000   41.0000;
   -1.0000   20.0000   41.0000;
    1.0000   20.0000   41.0000;
         0   45.0000         0;
   -1.0000   50.0000   37.0000;
    1.0000   50.0000   40.0000;
         0   20.0000         0;
   -1.0000   25.0000   40.0000;
    1.0000   25.0000   40.0000;
   -1.0000   25.0000   40.0000;
         0   60.0000         0];
seg_step= 50;      % Number of line segments per turn/straight (resolution)

% Track centerline coordinates
T_cl= zeros(seg_step*length(T(:,1))+1,3);  % Initialize matrix of coordinates
for i= 1:length(T(:,1))
    for j= 1:seg_step
        if T(i)==0          % straight case
            T_cl((i-1)*seg_step+j+1,1)= T_cl((i-1)*seg_step+j,1)+...
                T(i,2)/seg_step*cos(T_cl((i-1)*seg_step+j,3)*pi()/180);
            T_cl((i-1)*seg_step+j+1,2)= T_cl((i-1)*seg_step+j,2)+...
                T(i,2)/seg_step*sin(T_cl((i-1)*seg_step+j,3)*pi()/180);
            T_cl((i-1)*seg_step+j+1,3)= T_cl((i-1)*seg_step+j,3);
        elseif T(i)==1      % left turn case
            T_cl((i-1)*seg_step+j+1,1)= T_cl((i-1)*seg_step+j,1)+...
                T(i,2)*sqrt(2-2*cos(T(i,3)/seg_step*pi()/180))*...
                cos((T_cl((i-1)*seg_step+j,3)+T(i,3)/seg_step/2)*pi()/180);
            T_cl((i-1)*seg_step+j+1,2)= T_cl((i-1)*seg_step+j,2)+...
                T(i,2)*sqrt(2-2*cos(T(i,3)/seg_step*pi()/180))*...
                sin((T_cl((i-1)*seg_step+j,3)+T(i,3)/seg_step/2)*pi()/180);
            T_cl((i-1)*seg_step+j+1,3)= T_cl((i-1)*seg_step+j,3)+T(i,3)/...
                seg_step;
        elseif T(i)==-1     % right turn case
            T_cl((i-1)*seg_step+j+1,1)= T_cl((i-1)*seg_step+j,1)+...
                T(i,2)*sqrt(2-2*cos(T(i,3)/seg_step*pi()/180))*...
                cos((T_cl((i-1)*seg_step+j,3)-T(i,3)/seg_step/2)*pi()/180);
            T_cl((i-1)*seg_step+j+1,2)= T_cl((i-1)*seg_step+j,2)+...
                T(i,2)*sqrt(2-2*cos(T(i,3)/seg_step*pi()/180))*...
                sin((T_cl((i-1)*seg_step+j,3)-T(i,3)/seg_step/2)*pi()/180);
            T_cl((i-1)*seg_step+j+1,3)= T_cl((i-1)*seg_step+j,3)-T(i,3)/...
                seg_step;
        end
    end
end

%figure(1)
%hold on
%plot(T_cl(:,1),T_cl(:,2),'b-.')
%axis equal
%track_name= track(1:strfind(track,'.')-1);
%title(track_name)
%xlabel('(meters)')
%ylabel('(meters)')

% Left edge of track
T_L= zeros(length(T_cl),2);
T_L(:,1)= T_cl(:,1)-WoT/2*sin(T_cl(:,3)*pi()/180);
T_L(:,2)= T_cl(:,2)+WoT/2*cos(T_cl(:,3)*pi()/180);
%plot(T_L(:,1),T_L(:,2),'k')

% Right edge of track
T_R= zeros(length(T_cl),2);
T_R(:,1)= T_cl(:,1)+WoT/2*sin(T_cl(:,3)*pi()/180);
T_R(:,2)= T_cl(:,2)-WoT/2*cos(T_cl(:,3)*pi()/180);
%plot(T_R(:,1),T_R(:,2),'k')

% Draw start line
%plot([WoT/10,WoT/10],[-0.75*WoT-1,0.75*WoT+1],'k','LineWidth',2)
%plot([-WoT/10,-WoT/10],[-0.75*WoT-1,0.75*WoT+1],'k','LineWidth',2)


end