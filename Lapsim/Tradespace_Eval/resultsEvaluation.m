% evaluation of the results of the tradespace
clear all
close all
clc

pRank = 100;

filename = 'tradespaceResults.xlsx';
[results,~,~]  = xlsread(filename);

[m,n] = size(results)
top500 = zeros(pRank,17);
count = 1;
for i = 1:m
    if results(i,17) <= pRank
        top500(count,:) = results(i,:);
        count = count + 1;
    end
end

mostFreq = mode(top500)

figure(1)
plot(results(:,17),results(:,3),'.')
title('Mass vs Rank')
xlabel('Rank')
ylabel('Mass w/ driver - kg')

figure(2)
plot(results(:,14),results(:,17),'.')
title('Engine vs Rank')
xlabel('Engine Type')
ylabel('Rank')

figure(3)
plot(results(:,17),results(:,12),'.')
title('F Roll Stiffness vs Rank')
xlabel('Rank')
ylabel('Front Roll Stiffness -nm')

figure(4)
plot(results(:,17),results(:,13),'.')
title('R Roll Stiffness vs Rank')
xlabel('Rank')
ylabel('Rear Roll Stiffness -nm')

figure(5)
plot(results(:,17),results(:,5),'.')
title('Wheelbase vs Rank')
xlabel('Rank')
ylabel('Wheelbase - in')