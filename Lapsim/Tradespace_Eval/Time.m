function [t,t_c,t_s,Vaccel] = Time(RL,Vy_max,Vx_b,Vx_a,x_b,x_a,t_step,v_i)
% Calculates time in each section of lap using velocity vectors found
% previously

% Lengths of corners (straights are 0 corner length)
L_corner= abs(RL(:,1)).*RL(:,2).*RL(:,3)*(pi()/180);

% Lengths of straights (corners are 0 straight length)
L_straight= (1-abs(RL(:,1))).*RL(:,2);

% Initiate initial and final velocities
V_i= zeros(length(RL(:,1)),1);
V_f= zeros(length(RL(:,1)),1);

% Find initial and final velocities
for i= 1:length(RL(:,1))
    if i==1 && RL(i,1)==0   % condition that first section is a straight
        V_i(i)= v_i;        % initial velocity of first straight (m/s)
        % final velocity is the max speed in the next corner (m/s)
        V_f(i)= Vy_max(find(Vy_max~=Inf,1));
    % Condition that last section is a straight
    elseif i==length(RL(:,1)) && RL(i,1)==0
        V_i(i)= Vy_max(i-1);    % (m/s)
        V_f(i)= Inf;
    elseif RL(i,1)~=0   % condition that the section is a corner
        % initial and final velocities are the maximum cornering speed
        V_i(i)= Vy_max(i);      % (m/s)
        V_f(i)= Vy_max(i);      % (m/s)
    else
        % initial velocity is max cornering speed of previous corner
        V_i(i)= Vy_max(i-1);    % (m/s)
        % final velocity is max cornering speed of next corner
        V_f(i)= Vy_max(i+1);    % (m/s)
    end

end

% Calculate time in corners (s)
t_corner= L_corner./Vy_max;

% Calculate time in straights
% Finds intersection of acceleration and braking curves

% Initiate time vector
t_straight= zeros(length(RL(:,1)),1);

% Initiate intermediate terms
x_s= zeros(length(RL(:,1)),1);
k_i= zeros(length(RL(:,1)),1);
k_f= zeros(length(RL(:,1)),1);
x_i= zeros(length(RL(:,1)),1);
x_f= zeros(length(RL(:,1)),1);
k_xa= zeros(length(RL(:,1)),1);
k_xb= zeros(length(RL(:,1)),1);
m= zeros(length(RL(:,1)),1);
n= zeros(length(RL(:,1)),1);
strnumber = 0;

for i= 1:length(RL(:,1))
    if RL(i,1)~=0       % sets corners to 0
        t_straight(i)= 0;
    else
        strnumber = strnumber+1;
        x_s(i)= L_straight(i);
        k_i(i)= find(Vx_a >= V_i(i),1); % index of Vx_a for initial velocity
        x_i(i)= x_a(k_i(i));    % distance at initial velocity index
        k_xa(i)= find(x_a >= x_s(i)+x_i(i),1);  % index at end of acceleration
        
        % condition that next corner speed is higher than max speed
        % attained in straight
        if Vx_a(k_xa(i)) <= V_f(i)
            t_straight(i)= (k_xa(i)-k_i(i))*t_step;
        else
            k_f(i)= find(Vx_b >= V_f(i),1); % index of Vx_b for final velocity
            x_f(i)= x_b(k_f(i));    % distance at final velocity index
            k_xb(i)= find(x_b >= x_s(i)+x_f(i),1);  % index at end of braking
            
            m(i)= k_xb(i);    n(i)= k_i(i);    % initiate counters
            % Iteratively find intersection point of accel and decel curves
            while Vx_b(m(i)) >= Vx_a(n(i))
                m(i)= m(i)-1;
                xb= x_b(m(i))-x_b(k_f(i));
                xa= x_s(i)-xb;
                n(i)= find(x_a >= xa+x_i(i),1);
            end
            Vaccel(strnumber)=Vx_a(n(i));
            t_straight(i)= (n(i)-k_i(i)+m(i)-k_f(i))*t_step;
        end
    end
end

% Time vector of all section times (s)
t= t_corner+t_straight;
t_c= t_corner;
t_s= t_straight;


end
