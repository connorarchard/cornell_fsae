% Author: Connor Archard
% Date:   3/5/16
% Brief:  Tradespace Enumeration Script
close all
clear all
compFilename = 'tech_data_2013-2015.xlsx';
[num, text, compInformation] = xlsread(compFilename);

position  = num(:,1);
school    = text(:,2);
cylinders = num(:,3);
displace  = num(:,4); % in CC
weight    = num(:,5); % in KG
year      = num(:,6);
chassis   = text(:,7);
fAero     = num(:,8);
rAero     = num(:,9);
undertray = num(:,10);

numSchools = length(school)-1;

maxPosition = 50;

mCount = 0;
tCount = 0; 
hCount = 0;
count  = 0;

mWeight  = 0;
tWeight  = 0;
hWeight  = 0;

f15Weight   = 2.75; % 6lb
r15Weight   = 3.63; % 8lb
fWingWeight = 3.63; % 8lb
rWingWeight = 4.54; % 10lb
trayWeight  = 4.54; % 10lb

fitCylm  = [];
fitWeightm = [];
fitCylh  = [];
fitWeighth = [];
fitCylt  = [];
fitWeightt = [];

for i=1:numSchools
    if ((position(i) <= maxPosition)&&(weight(i)>0)&&(fAero(i)>0))
        count = count + 1;
        if (strcmp(chassis(i), 'm'))
            fitCylm = [fitCylm cylinders(i)];
            fitWeightm = [fitWeightm weight(i)];
            mCount = mCount + 1;
            mWeight = mWeight + weight(i);
            if (year(i) == 2015)
                if (fAero(i))
                    mWeight = mWeight - f15Weight;
                end
                if (rAero(i))
                    mWeight = mWeight - r15Weight;
                end
            else
                if (fAero(i))
                    mWeight = mWeight - fWingWeight;
                end
                if (rAero(i))
                    mWeight = mWeight - rWingWeight;
                end
            end
            if (undertray(i))
                mWeight = mWeight - trayWeight;
            end
        elseif (strcmp(chassis(i), 'h'))
            fitCylh = [fitCylh cylinders(i)];
            fitWeighth = [fitWeighth weight(i)];
            hCount = hCount + 1;
            hWeight  = hWeight + weight(i);
            if (year(i) == 2015)
                if (fAero(i))
                    hWeight = hWeight - f15Weight;
                end
                if (rAero(i))
                    hWeight = hWeight - r15Weight;
                end
            else
                if (fAero(i))
                    hWeight = hWeight - fWingWeight;
                end
                if (rAero(i))
                    hWeight = hWeight - rWingWeight;
                end
            end
            if (undertray(i))
                hWeight = hWeight - trayWeight;
            end
        elseif (strcmp(chassis(i), 't'))
            tCount = tCount + 1;
            tWeight  = tWeight + weight(i);
            fitCylt = [fitCylt cylinders(i)];
            fitWeightt = [fitWeightt weight(i)];
            if (year(i) == 2015)
                if (fAero(i))
                    tWeight = tWeight - f15Weight;
                end
                if (rAero(i))
                    tWeight = tWeight - r15Weight;
                end
            else
                if (fAero(i))
                    tWeight = tWeight - fWingWeight;
                end
                if (rAero(i))
                    tWeight = tWeight - rWingWeight;
                end
            end
            if (undertray(i))
                tWeight = tWeight - trayWeight;
            end
        end
    end       
end

avgM = mWeight/mCount
avgH = hWeight/hCount
avgT = tWeight/tCount

fprintf('%.1f percent of teams use a monocoque - a total of %i out of %i sampled\n',100*mCount/count , mCount, count)
fprintf('%.1f percent of teams use a hybrid frame - a total of %i out of %i sampled\n',100*hCount/count , hCount, count)
fprintf('%.1f percent of teams use a tube frame - a total of %i out of %i sampled\n',100*tCount/count , tCount, count)

m = polyfit(fitCylt,fitWeightt,2);
x = linspace(min(fitCylt),max(fitCylt));
y = m(1).*x.^2 + m(2).*x + m(3);%in(fitDisp);
figure(1)
hold on
plot(fitCylt,fitWeightt,'.')
plot(x,y,'k')
fprintf('Tube:\n weight = %.2f*x^2 + %.2f*x + %.2f\n\n',m(1),m(2),m(3));

m = polyfit(fitCylh,fitWeighth,1);
x = linspace(min(fitCylh),max(fitCylh));
y = m(1).*x + m(2);%in(fitDisp);
figure(2)
hold on
plot(fitCylh,fitWeighth,'.')
plot(x,y,'k')
fprintf('Hybrid:\n weight = %.2f*x + %.2f\n\n',m(1),m(2));

m = polyfit(fitCylm,fitWeightm,2);
x = linspace(min(fitCylm),max(fitCylm));
y = m(1).*x.^2 + m(2).*x + m(3);%in(fitDisp);
figure(3)
hold on
plot(fitCylm,fitWeightm,'.')
plot(x,y,'k')
fprintf('Monocoque:\n weight = %.2f*x^2 + %.2f*x + %.2f\n\n',m(1),m(2),m(3));

