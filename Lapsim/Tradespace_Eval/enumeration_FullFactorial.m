% -----------------------------------------------------------------------------
% Tradespace Enumeration Script
% Author: Connor Archard (cwa37@cornell.edu)
% Date:   3-13-16
% -----------------------------------------------------------------------------
clc
clear all
close all
% Chassis Parameters:
% [Vehicle Mass, Driver Mass, Wheelbase, Front Track Width, ...
%  Rear Track Width, Weight Dist, Front Roll Center Height, ...
%  Rear Roll Center Heigt, Front Roll Stiffness, ...
%  Rear Roll Stiffness]
Chassis = [];
Chassis(2) = 68; % driver mass
Chassis(4) = 1.206; % f track width
Chassis(5) = 1.194; % r track width
Chassis(6) = 0.264; % CG height
Chassis(8) = 0.0106; % Front roll center height
Chassis(9) = 0.0254; % Rear roll center height

% Aero Parameters:
% [Frontal Area, Body/Tire Lift Coeff, Body/Tire Drag Coeff, ...
%  Front Wing Lift Coeff, Front Wing Drag Coeff, Front Wing X Pos, ...
%  Front Wing Z Pos, Rear Wing Area, Rear Wing Lift Coeff, ...
%  Rear Wing Drag Coeff, Rear Wing X pos, Rear Wing z pos]
Aero = [];

% Tire Dimensions:
% [Front Rolling Radius, Rear Rolling Radius, Front Tire Width, ...
%  Rear Tire Width]
Tire_dim = [0.228, 0.228, 0.152, 0.152];

% Transmission:
% [1st ratio, 2nd ratio, 3rd ratio, 4th ratio, 5th ratio, 6th ratio, ...
%  Primary ratio, Final Drive ratio, Shift Down RPM, Shift Up RPM]
%
% Currently Set at stock values
Trans = [2.75,2,1.6666667,1.4444,1.304,1.208,2.111,2.364,6500,10000];

% Torque:
% First Column is RPM
% Second Column is Torque in N-m
%
% Read in the various xls files here and save them as torque curves to 
% be used later in selections

% Engine Options including aspiration options for eng_sel:
engType = cell([10 3]); % First column is ref to torque, 
                        % second column displacement
                        % third column # cylinders

% 1 - CBR600RR w/turbo
engType{1,1} = 'torque_curves/ARG16_CBR600RR.xlsx';
engType{1,2} = 600;
engType{1,3} = 4;

% 2 - CBR600RR w/o turbo
engType{2,1} = 'torque_curves/NA_CBR600RR.xlsx';
engType{2,2} = 600;
engType{2,3} = 4;

% 3 - CRF450 NA
engType{3,1} = 'torque_curves/NA_CRF450.xlsx';
engType{3,2} = 449;
engType{3,3} = 1;

% 4 - GXSR-600 NA
engType{4,1} = 'torque_curves/NA_GXSR-600.xlsx';
engType{4,2} = 600;
engType{4,3} = 4;

% 5 - KTM EXZR NA
engType{5,1} = 'torque_curves/NA_KTM_EXZR.xlsx';
engType{5,2} = -1;
engType{5,3} = -1;

% 6 - KTM LC4 640 NA
engType{6,1} = 'torque_curves/NA_KTM_LC4_640.xlsx';
engType{6,2} = 640;
engType{6,3} = 1;

% 7 - Ninja ZX6 NA
engType{7,1} = 'torque_curves/NA_NinjaZX6.xlsx';
engType{7,2} = 600;
engType{7,3} = 4;

% 8 - R6 NA
engType{8,1} = 'torque_curves/NA_R6.xlsx';
engType{8,2} = 600;
engType{8,3} = 4;

% 9 - WR450 NA
engType{9,1} = 'torque_curves/NA_WR450.xlsx';
engType{9,2} = 450;
engType{9,3} = 1;

% 10 - GXSR-600 Turbo 
engType{10,1} = 'torque_curves/Turbo_GXSR-600.xlsx';
engType{10,2} = 600;
engType{10,3} = 4;
engOptions = 5;

% Chassis Options for chassis_sel:
% 1 - monocoque
% 2 - hybrid
% 3 - tube frame
chassisOptions     = 3;
frontRollStiffness = linspace(540,660,4);
rearRollStiffness   = linspace(720,880,4);

% Aero Options for aero_sel:
% 1 - front and rear
% 2 - front
% 3 - rear
% 4 - no aero
aeroOptions = 4;

baseOptions = 4;
fRollOptions = 4;
rRollOptions = 4;
distOptions  = 1;
% Results:
% [T,t_accel,t_skid,V_section,count]
spaceSize = engOptions*chassisOptions*aeroOptions*baseOptions*fRollOptions*rRollOptions*distOptions
results   = -1*ones(spaceSize,5);
stats    = -1*ones(spaceSize,14);
iteration = 0; 
percentComplete = 0;
printTime = 1;
for eng_sel = 6:10
    % set the mass of the chassis and the torque curve in the following cases
    [Torque,~,~] = xlsread(engType{eng_sel,1});
    [rpm,IA,IC] = unique(Torque(:,1));
    nm          = Torque(IA,2);
    Torque = [rpm nm];
    eng_cc  = cell2mat(engType(eng_sel,2));
    eng_cyl = cell2mat(engType(eng_sel,3));
    for chassis_sel = 1:3
        switch (chassis_sel)
        case 1 % monocoque
        Chassis(1) = -12.78*eng_cyl^2 + 73.67*eng_cyl + 106.78;
        case 2 % hybrid
        Chassis(1) = 14.67*eng_cyl + 152.33;
        case 3 % tube
        Chassis(1) = -5.47*eng_cyl^2 + 42.96*eng_cyl + 127.2;
        otherwise
        fprintf('something has gone wrong. Chassis is unidentified.');
        end
        for f_roll_stiff = 1:length(frontRollStiffness)
            Chassis(10) = frontRollStiffness(f_roll_stiff);

            for r_roll_stiff = 1:length(rearRollStiffness)
                Chassis(11) = rearRollStiffness(r_roll_stiff);

                for aero_sel = 1:4
                    % set the additional mass and aero coeffs
                    switch(aero_sel)
                    case 1 % front and rear
                    Chassis(1) = Chassis(1) + 2.75 + 3.65;
                    Aero = [0.4 0 0.2 0.32 -2.6 0.24 0.6 0.1 0.38 -1.2 0.68 0 1.1];
                    case 2 % front
                    Chassis(1) = Chassis(1) + 2.75;
                    Aero = [0.4 0 0.2 0.32 -2.6 0.24 0.6 0.1 0 -1.2 0.68 0 1.1];
                    case 3 % rear
                    Chassis(1) = Chassis(1) + 3.65;
                    Aero = [0.4 0 0.2 0 -2.6 0.24 0.6 0.1 0.38 -1.2 0.68 0 1.1];
                    case 4 % no aero
                    Aero = [0.4 0 0.2 0 -2.6 0.24 0.6 0.1 0 -1.2 0.68 0 1.1];
                    otherwise
                    end          
                    for Wheelbase = 60:3:69
                        % set the Wheelbase
                        Chassis(3) = Wheelbase;
                        for weightDist = 0.58:0.04:0.59 %THIS IS INTENTIONALLY SETTING US AT 58% and then stopping
                            Chassis(7) = weightDist;
                            %increment iteration count and save results
                            iteration = iteration + 1;
                            tic
                            stats(iteration,:) = [Chassis eng_sel chassis_sel aero_sel];
                            results(iteration,:) = LapSim_Tradespace(Chassis, Aero, Tire_dim, ...
                                                                 Torque, Trans);
                            percentComplete = 100*iteration/spaceSize
                            timer = toc;
                            if printTime
                            fprintf('One evaluation took %.2f seconds.\n',timer)
                            fprintf('Total expected runtime is %.2f hours.\n',timer*spaceSize/3600)
                            printTime = 0;
                            end
                        end % Weight Distribution - 12
                    end % Wheelbase - 10
                end % aero_sel - 4
            end % r_roll_stiff - 10
        end % f_roll_stiff - 10
    end % chassis_sel - 3
end % eng_sel - 10

save('Tradespace_Results.mat')
