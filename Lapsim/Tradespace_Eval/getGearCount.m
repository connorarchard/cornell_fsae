function [count,Gr_f,RPM_section] = getGearCount(R_rr,V_max,Trans)

GR = 1;
count = 0;
gr_num= length(Trans)-4;            % Number of gears
shift_rpm = Trans(length(Trans));    % Shift RPM
down_shift= Trans(length(Trans)-1);
V_ini = 0;
V_max = [V_ini; V_max];
Gr_f = zeros(length(V_max),1);
RPM_section = zeros(length(V_max),1);

for i = 1:length(V_max)-1
    % shift up
    if V_max(i+1)> V_max(i)
        RPM= 60*gear_ratio(Trans,GR)*V_max(i+1)/(2*pi()*R_rr);
        while RPM >=shift_rpm && GR < gr_num
            GR= GR+1;
            count = count+1;
            RPM = 60*gear_ratio(Trans,GR)*V_max(i+1)/(2*pi()*R_rr);
        end
        RPM_section(i)= RPM; 
    end
    
    %shift down
    if V_max(i+1)< V_max(i)
        RPM= 60*gear_ratio(Trans,GR)*V_max(i+1)/(2*pi()*R_rr);
        while RPM <= down_shift && GR > 1
            GR=GR-1;
            count = count+1;
            RPM = 60*gear_ratio(Trans,GR)*V_max(i+1)/(2*pi()*R_rr);
        end
        RPM_section(i)= RPM; 
    end
    
    if V_max(i+1)== V_max(i)
        RPM_section(i)= RPM_section(i-1);
    end
    
    Gr_f(i)= GR; %final gear slection for this section
end
end
