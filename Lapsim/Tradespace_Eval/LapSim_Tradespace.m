function [T,t_accel,t_skid,V_section,count] = LapSim_Tradespace(Chassis,Aero,Tire_dim,Torque,Trans)
% Metric Units

% Set time interval of calculation (resolution) (sec)
t_step= 0.001;
% Set initial speed (0 m/s for launch) (m/s)
v_i= 0;

% Input car specification files
% Chassis= 'ARG15 Chassis.xlsx';      % Excel file of general/chassis specs
% [Chassis,~,~]= xlsread(Chassis);    % Extract specs from excel file
% Aero= 'ARG15_full_closed_aero.xlsx';            % Excel file of aero specs
% [Aero,~,~]= xlsread(Aero);          % Extract specs from excel file
% Tire_dim= 'ARG15 Tire Dim.xlsx';    % Excel file of tire dimensions
% [Tire_dim,~,~]= xlsread(Tire_dim);  % Extract dimensions from excel file
Tire_dim = [ 0.2280; 0.2280; 0.1520; 0.1520];
% Torque= 'Torque Curve_525.xlsx';  % Excel file of torque curve
% [Torque,~,~]= xlsread(Torque);      % Extract values from excel file
% Trans= 'ARG15 Transmission_stock.xlsx';   % Excel file of transmission specs
% [Trans,~,~]= xlsread(Trans);        % Extract specs from excel file

% Tire Model
% Excel file of tire model (ex. polyfit, pacejka)
%Tire_model= 'Hoosier 18x6-10 12psi_polyfit.xlsx';
% Extract values from excel file
%[Tire_model,~,~]= xlsread(Tire_model);
Tire_model =[   -0.0000   -0.0000   -0.0002    1.6526    0.0129;   0.0000   -0.0000   -0.0002    1.5430    0.6457];
% Input Track file
% track= 'MIS Autox 2013.xlsx';            % Excel file of track dimensions
% Extract track dimensions from excel file
% [T,~,~]= xlsread(track);

T= track_gen(0);          % Call track generation function
RL= T;                          % Racing line is the provided line 

% Solve for Maximum Corner Speeds
[Vy_max,Ay,Ayf,Ayr,Skid]= corner_max(RL,Chassis,Aero,Tire_model);

% Solve for Straight Line Acceleration & Velocity
[x_b,Vx_b,Ax_b,x_a,Vx_a,Ax_a,Fz_b]= straight_line(RL,...
    Chassis,Aero,Tire_model,Tire_dim,Torque,Trans,t_step);

% Solve for times in each section of lap (s)
[t,tc,ts,Vaccel]= Time(RL,Vy_max,Vx_b,Vx_a,x_b,x_a,t_step,v_i);

% Lap Time (s)
T= sum(t);

% Accel Time (75m straight) (s)
t_accel= find(x_a >= 75,1)/1000;

% Speed for each section
strnum = 0;
V_section = Vy_max(1:length(Vy_max)-1);
for z = 1:length(V_section)
    if V_section(z)== Inf
        strnum = strnum+1;
        V_section(z) = Vaccel(strnum);
    end
end

[count,gr_f,RPM_section] = getGearCount(Tire_dim(2),V_section,Trans); 

t_skid = Skid(1);

end