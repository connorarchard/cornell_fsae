function [Q]= torq_int(torque_curve,RPM)
% Interpolates torque output at a certain engine RPM given torque vs rpm 
% curve as a list of numbers

Torq= torque_curve;     % contains torque values for range of RPM
Q= interp1(RPM,Torq(:,2),Torq(:,1));    % interpolation function

end