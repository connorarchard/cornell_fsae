function [x_b,Vx_b,Ax_b,x_a,Vx_a,Ax_a,Fz_b,Loads_Braking, Loads_Accel_Traction]= straight_line(RL,...
    C,A,Tir,Tir_dim,Torque,Trans,t_step)
% calculates acceleration and speed curves for straight sections of the
% track from given track and car specifications

% Extract car specs from excel files
% General Specs
m= C(1);            % Car mass (kg)
m_driver= C(2);     % Driver mass (kg)
M= m+m_driver;      % Total mass (kg)
L= C(3);            % Wheel base (m)
tf= C(4);           % Front track width (m)
tr= C(5);           % Rear track width (m)
Hcg= C(6);          % CG height (m)
rwd= C(7);          % Rear weight distribution
fwd= 1-rwd;         % Front weight distribution
Hrf= C(8);          % Front roll center height (m)
Hrr= C(9);          % Rear roll center height (m)
Kf= C(10);          % Front roll stiffness (N*m/deg)
Kr= C(11);          % Rear roll stiffness (N*m/deg)

% Aero
S_body= A(1);       % Frontal area of car body including tires (m^2)
CL_body= A(2);      % Car body lift coefficient
CD_body= A(3);      % Car body drag coefficient
S_fw= A(4);         % Front wing area (m^2)
CL_fw= A(5);        % Front wing lift coefficient
CD_fw= A(6);        % Front wing drag coefficient
x_fw= A(7);         % x location of front wing center of pressure (m)
z_fw= A(8);         % z location of front wing center of pressure (m)
S_rw= A(9);         % Rear wing area (m^2)
CL_rw= A(10);       % Rear wing lift coefficient
CD_rw= A(11);       % Rear wing drag coefficient
x_rw= -A(12);       % x location of rear wing center of pressure (m)
z_rw= A(13);        % z location of rear wing center of pressure (m)

% Tires
Fx_c= Tir(2,:);     % Friction function
F_rr= Tir_dim(1);   % Front rolling radius (m)
R_rr= Tir_dim(2);   % Rear rolling radius (m)

% Torque Curve
Q_rpm= Torque(:,1);     % x axis of torque curve
Q_t= Torque(:,4);       % y axis of torque curve

% Transmission
gr_num= length(Trans)-3;            % Number of gears
shift_rpm= Trans(length(Trans));    % Shift RPM

g= 9.81;            % gravity (m/s^2)
rho= 1.225;         % air density (kg/m^3)

% Set max time in a straight (sec)
% Over estimate if unsure (accurate value will reduce run time)
t_max= 8;
% Time interval
t= 0:t_step:t_max;

% Initiate longitudinal speed and acceleration vectors
Vx_b= zeros(length(t),1);       % speed(t) vector for braking
Ax_b= zeros(length(t),1);       % accel(t) vector for braking
Vx_at= zeros(length(t),1);      % speed(t) vector for acceleration (traction limited)
Ax_at= zeros(length(t),1);      % accel(t) vector for acceleration (traction limited)
Vx_ae= zeros(length(t),1);      % speed(t) vector for acceleration (engine limited)
Ax_ae= zeros(length(t),1);      % accel(t) vector for acceleration (engine limited)

FF_b_tot= zeros(length(t),1);       % Front total load braking
FF_b_WT= zeros(length(t),1);       % Front weight transfer load braking
FF_b_Aero= zeros(length(t),1);       % Front aero load braking
RR_b_tot= zeros(length(t),1);       % Rear total load braking
RR_b_WT= zeros(length(t),1);       % Rear weight transfer braking
RR_b_Aero= zeros(length(t),1);       % Rear aero load braking

FF_at_tot= zeros(length(t),1);      % Front total load traction limited accel
FF_at_WT= zeros(length(t),1);       % Front weight transfer load traction limited accel
FF_at_Aero= zeros(length(t),1);     % Front aero load tractio limited accel
RR_at_tot= zeros(length(t),1);      % Rear total load tractio limited accel
RR_at_WT= zeros(length(t),1);       % Rear weight transfer load traction limited accel  
RR_at_Aero= zeros(length(t),1);     % Rear aero load traction limited accel

FF_ae_tot= zeros(length(t),1);      % Front total load engine limited accel
FF_ae_WT= zeros(length(t),1);       % Front weight transfer load engine limited accel
FF_ae_Aero= zeros(length(t),1);     % Front aero load engine limited accel
RR_ae_tot= zeros(length(t),1);      % Rear total load engine limited accel
RR_ae_WT= zeros(length(t),1);       % Rear weight transfer load engine limited accel
RR_ae_Aero= zeros(length(t),1);     % Rear aero load engine limited accel


% Braking
% Assumes that the car has braking power on all 4 tires and they are
% capable of providing enough braking torque to match the maximum
% longitudinal forces that the tires can generate

for i= 0:t_max/t_step
    
    Fzf= 0.5*fwd*M*g;   % Front static normal load (one tire) (N)
    Fzr= 0.5*rwd*M*g;   % Rear static normal load (one tire) (N)
    
    for j= 1:10
        Fxf= polyval(Fx_c,abs(Fzf));   % Front longitudinal force (N)
        Fxr= polyval(Fx_c,abs(Fzr));   % Rear longitudinal force (N)
        if Fxr<0        % Rear wheel lift
            Fxr= 0;
        end
        
        % Aero loads
        q= 0.5*rho*Vx_b(i+1)^2;         % dynamic pressure (pa)
        L_body= q*S_body*CL_body;       % body lift force (N)
        D_body= q*S_body*CD_body;       % body drag force (N)
        L_fw= q*S_fw*CL_fw;             % front wing lift force (N)
        D_fw= q*S_fw*CD_fw;             % front wing drag force (N)
        L_rw= q*S_rw*CL_rw;             % rear wing lift force (N)
        D_rw= q*S_rw*CD_rw;             % rear wing drag force (N)
        
        % Normal load on rear tires (without weight transfer) (N)
        Fzr= (L_fw*x_fw+D_fw*z_fw+D_body*Hcg+rwd*M*g*L+D_rw*z_rw...
            -L_body*rwd*L-L_rw*(x_rw+L))/L;
        % Normal load on front tires (without weight transfer) (N)
        Fzf= M*g-L_fw-L_rw-L_body-Fzr;
        
        % Longitudinal acceleration under braking (m/s^2)
        Ax_b(i+1)= (2*Fxf+2*Fxr+D_body+D_fw+D_rw)/M;
        
        % Longitudinal weight transfer
        long_wt_trans= Ax_b(i+1)*Hcg*M/L;
        
        % Normal load on one front tire
        Fzf= 0.5*(Fzf+long_wt_trans);
        % Normal load on one rear tire
        Fzr= 0.5*(Fzr-long_wt_trans);
        
        Fz_b(i+1,1)= 2*Fzf;
        Fz_b(i+1,2)= 2*Fzr;
        
    end
    FF_b_tot(i+1)= Fzf;       % Front total load braking
    FF_b_WT(i+1)= 0.5*fwd*M*g+0.5*long_wt_trans;       % Front weight transfer load braking
    FF_b_Aero(i+1)= FF_b_tot(i+1)-FF_b_WT(i+1);       % Front aero load braking
    
    RR_b_tot(i+1)= Fzr;       % Rear total load braking
    RR_b_WT(i+1)= 0.5*rwd*M*g-0.5*long_wt_trans;       % Rear weight transfer braking
    RR_b_Aero(i+1)= RR_b_tot(i+1)-RR_b_WT(i+1);       % Rear aero load braking
    
    Vx_b(i+2)= Vx_b(i+1)+Ax_b(i+1)*t_step;
    
end

Loads_Braking (1,:) = FF_b_tot./g;
Loads_Braking (2,:) = FF_b_WT./g;
Loads_Braking (3,:) = FF_b_Aero./g;

Loads_Braking (4,:) = RR_b_tot./g;
Loads_Braking (5,:) = RR_b_WT./g;
Loads_Braking (6,:) = RR_b_Aero./g;

x_b= zeros(length(t),1);

for i= 0:t_max/t_step-1
    x_b(i+2)= x_b(i+1)+Vx_b(i+1)*t_step;
end


% Acceleration -- Traction Limited Case
% Assumes that the car has drive on 2 rear tires
for i= 0:t_max/t_step
    Fzr= 0.5*rwd*M*g;   % Rear static normal load (one tire) (N)
    Fzf= 0.5*fwd*M*g;   % Front static normal load (one tire) (N)
    
    for j= 1:10
        Fxr= polyval(Fx_c,abs(Fzr));   % Rear longitudinal force (N)
        Fxf= polyval(Fx_c,abs(Fzf));   % Front longitudinal force (N)
        
        % Aero loads
        q= 0.5*rho*Vx_at(i+1)^2;        % dynamic pressure (pa)
        L_body= q*S_body*CL_body;       % body lift force (N)
        D_body= q*S_body*CD_body;       % body drag force (N)
        L_fw= q*S_fw*CL_fw;             % front wing lift force (N)
        D_fw= q*S_fw*CD_fw;             % front wing drag force (N)
        L_rw= q*S_rw*CL_rw;             % rear wing lift force (N)
        D_rw= q*S_rw*CD_rw;             % rear wing drag force (N)
        
        % Normal load on rear tires (without weight transfer) (N)
        Fzr= (L_fw*x_fw+D_fw*z_fw+D_body*Hcg+rwd*M*g*L+D_rw*z_rw...
            -L_body*rwd*L-L_rw*(x_rw+L))/L;
        
        % Normal load on front tires (without weight transfer) (N)
        Fzf= M*g-L_fw-L_rw-L_body-Fzr;
        
        % Longitudinal acceleration (m/s^2)
        Ax_at(i+1)= (2*Fxr-D_body-D_fw-D_rw)/M;
        
        % Longitudinal weight transfer
        long_wt_trans= Ax_at(i+1)*Hcg*M/L;
        
        % Normal load on one rear tire
        Fzr= 0.5*(Fzr+long_wt_trans);
        % Normal load on one front tire
        Fzf= 0.5*(Fzf-long_wt_trans);        
    end
    
    % Longitudinal speed (m/s)
    Vx_at(i+2)= Vx_at(i+1)+Ax_at(i+1)*t_step;
    
    FF_at_tot(i+1)= Fzf;       % Front total load braking
    FF_at_WT(i+1)= 0.5*fwd*M*g-0.5*long_wt_trans;       % Front weight transfer load braking
    FF_at_Aero(i+1)= FF_b_tot(i+1)-FF_b_WT(i+1);       % Front aero load braking
    
    RR_at_tot(i+1)= Fzr;       % Rear total load braking
    RR_at_WT(i+1)= 0.5*rwd*M*g+0.5*long_wt_trans;       % Rear weight transfer braking
    RR_at_Aero(i+1)= RR_b_tot(i+1)-RR_b_WT(i+1);       % Rear aero load braking
end

Loads_Accel_Traction (1,:) = FF_at_tot./g;
Loads_Accel_Traction (2,:) = FF_at_WT./g;
Loads_Accel_Traction (3,:) = FF_at_Aero./g;

Loads_Accel_Traction (4,:) = RR_at_tot./g;
Loads_Accel_Traction (5,:) = RR_at_WT./g;
Loads_Accel_Traction (6,:) = RR_at_Aero./g;

% Acceleration -- Engine Limited Case
% Assumes that the car has drive on 2 rear tires

% Initiate gear variable
GR= 1;

for i= 0:t_max/t_step
    % Engine rpm for given vehicle speed
    % Start at minimum speed dictated by minimum RPM on torque curve
    if i==0
        RPM= min(Q_rpm);
    else
        RPM= 60*gear_ratio(Trans,GR)*Vx_ae(i+1)/(2*pi()*R_rr);
    end
    % Look up torque value from torque curve (N*m)
    Q_eng= interp1(Q_rpm,Q_t,RPM,'cubic');
    
    % Torque at rear tires (assumes no drive train losses) (N*m)
    Q_tir= Q_eng*gear_ratio(Trans,GR);
    % Longitudinal force on rear tires (N)
    Fxr= Q_tir/R_rr;
    
    % Drag loads
    q= 0.5*rho*Vx_ae(i+1)^2;        % dynamic pressure (pa)
    D_body= q*S_body*CD_body;       % body drag force (N)
    D_fw= q*S_fw*CD_fw;             % front wing drag force (N)
    D_rw= q*S_rw*CD_rw;             % rear wing drag force (N)
    
    % Longitudinal acceleration (m/s^2)
    Ax_ae(i+1)= (Fxr-D_body-D_fw-D_rw)/M;

    % Longitudinal speed (m/s)
    Vx_ae(i+1)= 2*pi()*R_rr*RPM/(60*gear_ratio(Trans,GR));
    
    % Shift up to next gear if above shifting RPM
    if RPM >= shift_rpm && GR < gr_num
        GR= GR+1;
        Vx_ae(i+2)= Vx_ae(i+1)+Ax_ae(i+1)*t_step;
    % Shifting RPM is reached in final gear:
    elseif RPM >= shift_rpm && GR == gr_num
        Vx_ae(i+2)= Vx_ae(i+1);
    else
        Vx_ae(i+2)= Vx_ae(i+1)+Ax_ae(i+1)*t_step;
    end
    
end

% Combine traction limited curve with engine limited curve
% Assumes curves start traction limited
k= 1;       % Initiate counter
while Vx_at(k) <= Vx_ae(1)
    Vx_a(k)= Vx_at(k);
    Ax_a(k)= Ax_at(k);
    k= k+1;     % Advance counter
end

j= k;       % New counter
while Ax_at(j)*Vx_at(j) < Ax_ae(j-k+1)*Vx_ae(j-k+1)
    Vx_a(j)= Vx_at(j);
    Ax_a(j)= Ax_at(j);
    j= j+1;     % Advance counter
end

if j == k
    j=j+1;
end

z= length(Ax_at);
for i= j:z
    if Ax_ae(i-k) < Ax_at(i)
        Ax_a(i)= Ax_ae(i-k);
        Vx_a(i)= Vx_ae(i-k);
    else
        Ax_a(i)= Ax_at(i);
        Vx_a(i)= Vx_at(i);
    end
end

x_a= zeros(length(Ax_a),1);

for i= 1:length(Ax_a)-1
    x_a(i+1)= x_a(i)+Vx_a(i)*t_step;
end

figure(2)
hold on
plot(Vx_ae(1:length(Vx_ae)-1)*2.237,Ax_ae/9.81,'r')
plot(Vx_at(1:length(Vx_at)-1)*2.237,Ax_at/9.81)
xlabel('Velocity (mph)')
ylabel('Longitudinal Acceleration (g)')
legend('engine', 'tire')

figure (5)
hold on
title ('Total load on tires during braking')
xlabel('speed (m/s)')
ylabel('Total load (kg)')
plot (Vx_b(1:3000),Loads_Braking (1,1:3000),'k')
plot (Vx_b(1:3000),Loads_Braking (4,1:3000),'r')
legend ('Front','Rear','Location','Best')

figure (6)
hold on
title ('Total loads on tires during acceleration (Traction limit case)')
xlabel('Speed (m/s)')
ylabel('Total load (kg)')
plot (Vx_at(1:4000),Loads_Accel_Traction (1,1:4000),'k')
plot (Vx_at(1:4000),Loads_Accel_Traction (4,1:4000),'r')
legend ('Front','Rear','Location','Best')

end