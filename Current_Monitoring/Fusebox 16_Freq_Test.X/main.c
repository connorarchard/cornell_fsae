/* 
 * File:   main.c
 * Author: Dhruv Gaba
 *
 * Created on February 19, 2016, 3:24 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <p18F2585.h>
#include <math.h>
#include <string.h>
#include <xc.h>

#pragma config OSC =  IRCIO7 // IRCIO67//IRCIO7  //this now enables high speed oscillator //IRCIO7 <- This was the old value for internal with ouput enabled

#pragma config PWRT = ON//// TURNS ON power on timer to allow psu to be stabl

#define _XTAL_FREQ 16000000
#define LED LATCbits.LC0
#define CHECK LATBbits.LATB0

void initialize(void) {

    /* USE FOR INTERNAL OSCILLATOR ONLY*/
    OSCTUNEbits.INTSRC = 1;     //31.25 kHz device clock derived from 8 MHz INTOSC source (divide-by-256 enabled
    OSCCONbits.IRCF0 = 0;          //4 MHz (INTOSC drives clock directly)
    OSCCONbits.IRCF1 = 1;
    OSCCONbits.IRCF2 = 1;

    OSCCONbits.SCS1 = 0;
    OSCCONbits.SCS0 = 0;

    while(OSCCONbits.OSTS == 0){}    //Oscillator Start-up Time-out Status bit
    while(OSCCONbits.IOFS == 0){}    //INTOSC Frequency Stable bit

    OSCTUNEbits.PLLEN = 1;         //PLL enabled for INTOSC (4 MHz and 8 MHz only)

    while(OSCCONbits.IOFS == 0){}    //INTOSC Frequency Stable bit

    TRISC = 0x00;
    TRISB = 0x08;
    LED = 0b0;
    }

int main(int argc, char** argv) {
    initialize();
    while(1)
    {
        LED^=1;
    }
    return (EXIT_SUCCESS);
}

