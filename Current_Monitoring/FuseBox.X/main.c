/* 
 * File:   ARG16
 * Author: Dhruv Gaba
 *
 */

#include <stdio.h> // standard io
#include <stdlib.h> // standard library
#include <p18F2585.h>
//#include <p18f25k80.h> // Not needed when xc.h is used
// Make sure project is setup to use the correct chip in MPLAB X
#include <xc.h> //compiler
#include "can18xx8.h" // CAN library
// pragma- set any hardware
#pragma config OSC = IRCIO67    // Oscillator (Internal RC oscillator)
//#pragma config PLLCFG = ON      // PLL x4 Enable bit (Enabled)
#pragma config PWRT = ON // TURNS ON power on timer to allow psu to be stable
//#pragma config CANMX = PORTB // Sets CAN output on RB2 and RB3 (register ports)
#pragma config XINST = OFF
//#pragma config WDTEN = OFF // Watchdog timer - resets if something's inactive

#define _XTAL_FREQ 16000000 // define frequency to run at (16 MHz)
#define AN0 0b00000 // renames 0b00000 as AN0 for coding purposes
#define AN1 0b00001
#define AN2 0b00010
#define AN3 0b00011
#define AN4 0b00100

#define CMON0ZERO 2045
#define CMON1ZERO 2050
#define CMON2ZERO 2040
#define CMON3ZERO 2044

#define CMON0MP 12
#define CMON1MP -20
#define CMON2MP -7
#define CMON3MP 9

int results[5] = {0,0,0,0,0};
unsigned char inputs[5] = {AN0, AN1, AN2, AN3, AN4};

unsigned int CAN_timer; // in mS

//void initConfig(){
//    // 0bxxx doesn't matter what the number before "b" is
//    OSCCONbits.SCS = 0b10; // Use internal OSC
//    OSCCONbits.IRCF = 0b111; // Sets internal clock to 4MHz
//    OSCTUNEbits.PLLEN = 0b1; // Multiplies internal clock by 4 to get 16MHz
//}

void initTimers() {

    OSCTUNEbits.INTSRC = 1;     //31.25 kHz device clock derived from 8 MHz INTOSC source (divide-by-256 enabled
    OSCCONbits.IRCF0 = 0;          //4 MHz (INTOSC drives clock directly)
    OSCCONbits.IRCF1 = 1;
    OSCCONbits.IRCF2 = 1;

    OSCCONbits.SCS = 0;

    while(OSCCONbits.OSTS == 0){}    //Oscillator Start-up Time-out Status bit
    while(OSCCONbits.IOFS == 0){}    //INTOSC Frequency Stable bit

    OSCTUNEbits.PLLEN = 1;         //PLL enabled for INTOSC (4 MHz and 8 MHz only)

    while(OSCCONbits.IOFS == 0){}    //INTOSC Frequency Stable bit

    //Initialize Timer0 to count ms
    T0CON = 0x00; // Reset Timer0 to 16 bit, use prescaler, use instruction clk
    T0CONbits.T0PS = 0b111; //1:256 prescale; 16mhz/4/256 = 31250/2hz
    TMR0H = 0xFD; // Rollover in 625 cycles
    TMR0L = 0x8E; // Results in 25hz interrupt (40ms)
    T0CONbits.TMR0ON = 0b1; // Turn timer on

    INTCONbits.GIE = 0b1; // Global interrupt enable
    INTCONbits.TMR0IF = 0b0; // Clear timer0 overflow interrupt bit
    INTCONbits.TMR0IE = 0b1; // Enable timer0 overflow interrupt
}

void initAnalog() {
    PORTA = 0x00; // Clear Port A output latches
    //TRISx sets as input or output (1=input, 0=output)
    TRISAbits.TRISA0 = 0b1; // Set RA0 as input
    TRISAbits.TRISA1 = 0b1; // Set RA1 as input
    TRISAbits.TRISA2 = 0b1; // Set RA2 as input
    TRISAbits.TRISA3 = 0b1; // Set RA3 as input
   // ANCON0 = 0b00001111; // Set RA0 to RA3 as analog inputs
    ADCON0 = 0x00; // Reset ADC select bits
    ADCON0bits.CHS = AN0; // Initialize analog input to AN0
    ADCON1bits.VCFG = 0b00; // Use AVdd
    ADCON2bits.ADFM = 0b0; // Left justified result
    ADCON2bits.ACQT = 0b111; // Acquisition time of 20Tad
    ADCON2bits.ADCS = 0b101; // Taq = 16 Tosc
    ADCON0bits.ADON = 0b1; // Turn ADC on
}


//void initCAN() {
//    PORTB = 0x00; // Clear Port B output latches
//    TRISBbits.TRISB2 = 0b0; //Set RB2 as output for CANTX
//    TRISBbits.TRISB1 = 0b1; //Set RB3 as input for CANRX
//    short init_flag = CAN_CONFIG_SAMPLE_ONCE & CAN_CONFIG_PHSEG2_PRG_ON &
//    CAN_CONFIG_DBL_BUFFER_ON &CAN_CONFIG_ALL_MSG & CAN_CONFIG_LINE_FILTER_OFF; // when we initialize CAN it knows what settings to initialize to
//    CANInitialize(1, 1, 3, 2, 2,init_flag);
//    CIOCON = 0b00100000; // set up register for CAN
//}

void initCAN(void) {
    TRISC = 0x00;
    TRISB = 0x08;
    short init_flag = CAN_CONFIG_SAMPLE_ONCE & CAN_CONFIG_PHSEG2_PRG_ON & CAN_CONFIG_DBL_BUFFER_ON &CAN_CONFIG_ALL_MSG & CAN_CONFIG_LINE_FILTER_OFF;
    CANInitialize(1, 1, 3, 2, 2,init_flag);
    //CANSetOperationMode(CAN_OP_MODE_CONFIG, 0xFF);
    //short mask = -1;
    //CANSetMask(CAN_MASK_B1, mask, CAN_CONFIG_STD_MSG)
    CIOCON = 0x20;
}

void sendCANMain() { // what is the current
    BYTE MessageData[8];
    for (int i = 0; i<4; i++){
        MessageData[i*2+1] = results[i] & 0xFF; //Convert 16 bit int into two 8 bits
        MessageData[i*2] = (results[i] >> 8);

    }

    if(CANIsTxReady()){
        CANSendMessage(0x007,MessageData,8,
                CAN_TX_PRIORITY_0 & CAN_TX_STD_FRAME & CAN_TX_NO_RTR_FRAME);
    }
}

void sendCANBatt() { // what is battery voltage (battery monitoring)
    BYTE MessageData[2];
    MessageData[1] = results[4] & 0xFF; //Convert 16 bit int into two 8 bits
    MessageData[0] = (results[4] >> 8);

    if(CANIsTxReady()){
        CANSendMessage(0x008,MessageData,2,
                CAN_TX_PRIORITY_0 & CAN_TX_STD_FRAME & CAN_TX_NO_RTR_FRAME);

    }
}

void getAnalog() {
    for(int i = 0; i < 5; i++) {
        ADCON0bits.CHS = inputs[i]; // Set which input to read from
        ADCON0bits.GODONE = 0b1; // Start acquiring analog input
        while(ADCON0bits.GODONE); // Wait until acquisition finishes
        results[i] = (ADRESH<<4) + (ADRESL>>4); // Combine into 16 bit unsigned int
    }
    results[0] = (results[0] - CMON0ZERO)*CMON0MP;
    results[1] = (results[1] - CMON1ZERO)*CMON1MP;
    results[2] = (results[2] - CMON2ZERO)*CMON2MP;
    results[3] = (results[3] - CMON3ZERO)*CMON3MP;
    //results[4] = results[4] - CMON0ZERO;
}

int main(int argc, char** argv) {
    //initConfig();
    initAnalog();
    initTimers();
    initCAN();
    TRISC = 0x00;
    while(1) {
        //do some stuff
        //getAnalog(); Now done in interrupt
    }
    return (EXIT_SUCCESS);
}

void interrupt ISR(void) {
    if(INTCONbits.TMR0IF && INTCONbits.TMR0IE) {
        INTCONbits.TMR0IF = 0b0; // Reset interrupt flag
        T0CONbits.TMR0ON = 0b0; // Turn timer off to reset count register
        // Reset the timer count
        TMR0H = 0xFD;
        TMR0L = 0x8E;
        T0CONbits.TMR0ON = 0b1; // Turn timer back on
        getAnalog();
        sendCANMain();
        //sendCANBatt();
    }
}